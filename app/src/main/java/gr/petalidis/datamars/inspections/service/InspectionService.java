/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import org.slf4j.Logger;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.domain.ScannedDocument;
import gr.petalidis.datamars.inspections.dto.InspectionDateProducer;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.repository.EntryRepository;
import gr.petalidis.datamars.inspections.repository.InspectionRepository;
import gr.petalidis.datamars.inspections.repository.OtherEntryRepository;
import gr.petalidis.datamars.inspections.repository.ScannedDocumentRepository;

public class InspectionService {
    private static final Logger log = Log4jHelper.getLogger(InspectionService.class.getName());

    public static List<InspectionDateProducer> findAllInspections(DbHandler dbHandler) throws PersistenceException
    {
        try {
            return InspectionRepository.getAllInspections(dbHandler);
        } catch (Exception e) {
            log.error("findAllInspections(): Unable to get inspection: {}", e.getLocalizedMessage());
            throw new PersistenceException(e.getLocalizedMessage());
        }
    }
    public static Inspection findInspectionFor(DbHandler dbHandler, String uuidString) throws PersistenceException
    {
        try {
            return InspectionRepository.getInspectionFor(dbHandler,uuidString);
        } catch (Exception e) {
            log.error("findInspectionFor(): Unable to get inspection: {}", e.getLocalizedMessage());
            throw new PersistenceException(e.getLocalizedMessage());
        }
    }

    public static boolean delete(DbHandler dbHandler, String inspectionIdString) {
        UUID inspectionId = UUID.fromString(inspectionIdString);
        EntryRepository.deleteWithInspectionId(dbHandler, inspectionId);
        OtherEntryRepository.deleteWithInspectionId(dbHandler, inspectionId);
        ScannedDocumentRepository.deleteWithInspectionId(dbHandler, inspectionId);
        InspectionRepository.delete(dbHandler, inspectionId);
        return true;
    }

    public static Inspection merge(DbHandler dbHandler, Inspection inspection) throws PersistenceException {
        if (inspection.isFinalised()) {
            EntryRepository.deleteInvalidEntriesFromInspection(dbHandler, inspection.getId(), inspection.getInvalidEntries());
        }

        InspectionRepository.save(dbHandler, inspection);
        EntryRepository.update(dbHandler, inspection.getEntries());
        OtherEntryRepository.save(dbHandler, inspection.getConventionalTags());
        ScannedDocumentRepository.save(dbHandler, inspection.getScannedDocuments());
        return inspection;
    }

    public static Inspection save(DbHandler dbHandler, Inspection inspection) throws PersistenceException {
        if (inspection.isFinalised()) {
            EntryRepository.deleteInvalidEntriesFromInspection(dbHandler, inspection.getId(), inspection.getInvalidEntries());
        }
        if (InspectionRepository.hasInspectionWithId(dbHandler, inspection.getId().toString())) {
            return update(dbHandler, inspection);
        }
        InspectionRepository.save(dbHandler, inspection);
        EntryRepository.save(dbHandler, inspection.getEntries());
        OtherEntryRepository.save(dbHandler, inspection.getConventionalTags());
        ScannedDocumentRepository.save(dbHandler, inspection.getScannedDocuments());
        return inspection;
    }

    public static boolean updateUploaded(DbHandler dbHandler, UUID inspectionId) throws PersistenceException {
        return InspectionRepository.updateUploadedTrue(dbHandler, inspectionId);
    }

    private static Inspection update(DbHandler dbHandler, Inspection inspection) throws PersistenceException {
        InspectionRepository.update(dbHandler, inspection);
        EntryRepository.update(dbHandler, inspection.getEntries());
        OtherEntryRepository.deleteWithInspectionId(dbHandler, inspection.getId());
        OtherEntryRepository.save(dbHandler, inspection.getConventionalTags());//We always recreate them?
        ScannedDocumentRepository.update(dbHandler, inspection.getScannedDocuments());
        return inspection;
    }

    public static Inspection updatePhotos(DbHandler dbHandler, Inspection inspection, List<ScannedDocument> scannedDocuments) {

        ScannedDocumentRepository.save(dbHandler, scannedDocuments);
        inspection.getScannedDocuments().addAll(scannedDocuments);
        return inspection;
    }

    public static void updateLocation(DbHandler dbHandler, UUID inspectionId, double latitude, double longitude)
            throws PersistenceException {

        InspectionRepository.updateLocation(dbHandler, inspectionId, latitude, longitude);
    }

    public List<String> getCsvString(DbHandler dbHandler, String uuidString) throws PersistenceException {
        try {
            Inspection inspection = InspectionRepository.getInspectionFor(dbHandler, uuidString);
            return inspection.toStrings();
        } catch (ParseException e) {
            log.error("getCsvString(): Unable to get inspection: {}", e.getLocalizedMessage());
            throw new PersistenceException(e.getLocalizedMessage());
        }
    }

    public static void finaliseInspections(DbHandler dbHandler, final Inspection inspection) throws PersistenceException {
        try {
            inspection.setFinalised(true);
            List<Inspectee> producersWithNoDummy = inspection.getProducersWithNoDummy();
            List<Inspection> otherInspections = producersWithNoDummy.stream().filter(x -> !x.getTin().equals(inspection.getProducer1Tin()))
                    .map(x -> inspection.generateInspectionFor(x)).collect(Collectors.toList());
            inspection.clearOtherInspectees();
            InspectionService.save(dbHandler, inspection);
            otherInspections.forEach(x -> {
                try {
                    InspectionService.merge(dbHandler, x);
                } catch (PersistenceException e) {
                    log.error("Αποτυχία αποθήκευσης ελέγχου {}. Λόγος {}", x.getProducer1Tin(), e.getMessage());
                }
            });

        } catch (PersistenceException e) {
            log.error("Αποτυχία αποθήκευσης ελέγχου {}. Λόγος {}", inspection.getProducer1Tin(), e.getMessage());

        }
    }
}
