/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import org.slf4j.Logger;

import java.util.Comparator;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.Entry;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;

public class InspectionStepThreeActivity extends AppCompatActivity {

    private static final String INSPECTION_KEY = "inspection";
    private Inspection inspection;
    private static final Logger log = Log4jHelper.getLogger(InspectionStepThreeActivity.class.getName());

    private Drawable currentDrawable = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // recovering the instance state
        if (savedInstanceState != null) {
            inspection = (Inspection) savedInstanceState.getSerializable(INSPECTION_KEY);
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable(INSPECTION_KEY);
        }
        setContentView(R.layout.activity_inspection_step_three);

        final Button checkButton = findViewById(R.id.ischecked);

        checkButton.setOnLongClickListener(this::reverseSelection);

        final ListView listview = findViewById(R.id.editItemsList);

        final InspectionStepThreeAdapter adapter = new InspectionStepThreeAdapter(this,
                android.R.layout.simple_list_item_1, inspection.getProducers(), inspection.getEntries());

        listview.setAdapter(adapter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InspectionStepTwoActivity.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INSPECTION_KEY, inspection);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.temp_save:

                if (inspection != null) {
                    DbHandler dbHandler = new DbHandler(Moo.getAppContext());
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                                //Do Nothing
                            });
                    try {
                        inspection = InspectionService.save(dbHandler, inspection);
                        builder.setTitle(R.string.success).setMessage(R.string.success);
                        builder.show();
                    } catch (PersistenceException e) {
                        builder.setTitle(R.string.failure).setMessage(e.getMessage());
                        builder.show();
                    }
                }
                return true;
            case R.id.about:
                PackageManager packageManager = this.getPackageManager();
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                            //Do Nothing
                        });
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                    String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                            "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                            "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                    builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                    builder.show();
                } catch (PackageManager.NameNotFoundException e) {
                    log.error("Could not show about message {}", e.getLocalizedMessage());
                }

                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goToStepFourActivity(View view) {
        Intent intent = new Intent(this, InspectionCohabitants.class);

        intent.putExtra(INSPECTION_KEY, inspection);

        startActivity(intent);

    }

    private void resetDrawables() {
        Button button = findViewById(R.id.tag);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.age);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.type);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.names);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.ischecked);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.race);
        button.setCompoundDrawables(null, null, null, null);
        button = findViewById(R.id.viewCommentsButton);
        button.setCompoundDrawables(null, null, null, null);
    }

    public void sortGridEntriesByTag(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getTag));
        Button button = findViewById(R.id.tag);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getTag).reversed());
            button.setOnClickListener(this::sortGridEntriesByTag);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    public void sortGridEntriesByTime(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getTagDate));
        Button button = findViewById(R.id.age);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getTagDate).reversed());
            button.setOnClickListener(this::sortGridEntriesByTime);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    public void sortGridEntriesByAnimalType(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getAnimalType));
        Button button = findViewById(R.id.type);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getAnimalType).reversed());
            button.setOnClickListener(this::sortGridEntriesByAnimalType);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    public void sortGridEntriesByProducer(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getProducer));
        Button button = findViewById(R.id.names);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getProducer).reversed());
            button.setOnClickListener(this::sortGridEntriesByProducer);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    public void sortGridEntriesByIsInRegister(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::isInRegister));
        Button button = findViewById(R.id.ischecked);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::isInRegister).reversed());
            button.setOnClickListener(this::sortGridEntriesByIsInRegister);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    public void sortGridEntriesByGenre(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getAnimalGenre));
        Button button = findViewById(R.id.race);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionStepThreeAdapter.sort(Comparator.comparing(Entry::getAnimalGenre).reversed());
            button.setOnClickListener(this::sortGridEntriesByGenre);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        });
    }

    private boolean reverseSelection(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionStepThreeAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionStepThreeAdapter.revertRegister();
        inspectionStepThreeAdapter.notifyDataSetChanged();
        gridView.setAdapter(inspectionStepThreeAdapter);
        return true;
    }

    public void sortGridEntriesByComments(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionViewAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        inspectionViewAdapter.sort(Comparator.comparing(Entry::getComment));
        Button button = findViewById(R.id.viewCommentsButton);
        resetDrawables();
        currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_up_12, null);
        button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);
        button.setOnClickListener(v ->
        {
            inspectionViewAdapter.sort(Comparator.comparing(Entry::getComment).reversed());
            button.setOnClickListener(this::sortGridEntriesByComments);
            resetDrawables();
            currentDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_baseline_arrow_drop_down_12, null);
            button.setCompoundDrawablesWithIntrinsicBounds(currentDrawable, null, null, null);

        });
    }

    public void addEntry(View view) {
        ListView gridView = findViewById(R.id.editItemsList);
        InspectionStepThreeAdapter inspectionViewAdapter = (InspectionStepThreeAdapter) gridView.getAdapter();
        Entry entry = inspection.createNewEntry();
        resetDrawables();
        inspectionViewAdapter.insert(entry, 0);
    }
}
