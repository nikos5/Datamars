
/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.domain;

import java.io.Serializable;

public enum OtherEntryType implements Serializable {
    CONVENTIONAL("CONVENTIONAL", ">=2010"),
    SINGLE("SINGLE", "Μονά"),
    ILLEGAL("ILLEGAL", "<2010"),
    NO_EARRING("NO_EARRING", "Χωρίς ενώτια"),
    NO_EARRING_UNDER_6("NO_EARRING_UNDER_6", "Χωρίς ενώτια(<6 μηνών)"),
    NO_EARRING_UNDER_6_OUT_OF_REGISTRY("NO_EARRING_UNDER_6_OUT_OF_REGISTRY", "Χωρίς ενώτια εκτός μητρώου (<6 μηνών)"),
    NO_EARRING_OVER_6("NO_EARRING_OVER_6", "Χωρίς ενώτια (>6 μηνών)"),
    OUT_OF_REGISTRY("OUT_OF_REGISTRY", "Εκτός μητρώου");
    private final String name;

    public final String label;

    OtherEntryType(String name, String label) {
        this.label = label;

        this.name = name;
    }

    public String getName() {
        return name;
    }
}
