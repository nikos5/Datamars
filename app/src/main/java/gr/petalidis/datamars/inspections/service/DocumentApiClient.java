/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;

import java.io.IOException;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.inspections.dto.inspection.DocumentDto;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class DocumentApiClient {
    static final String BASE_URL = Moo.getProperty("server");//"http://10.0.2.2:8090";
    private static final Logger log = Log4jHelper.getLogger(DocumentApiClient.class.getName());

    public static boolean createDocument(String jwt, DocumentDto documentDto) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper()))
                .build();

        DocumentApi documentApi = retrofit.create(DocumentApi.class);


        Call<DocumentDto> call = documentApi.create(jwt, documentDto);

        Response<DocumentDto> response = call.execute();

        if (response.isSuccessful()) {
            return true;
        } else {
            log.error("Tried to post document with id {}. Got back {}", documentDto.getId(), response.code());
            throw new IOException("Got back error code: " + response.code());
        }
    }

}