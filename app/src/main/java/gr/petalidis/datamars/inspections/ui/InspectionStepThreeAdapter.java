/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.CommentType;
import gr.petalidis.datamars.inspections.domain.Entry;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.ui.fragments.EditTagDialog;
import gr.petalidis.datamars.inspections.utilities.TagFormatter;

class InspectionStepThreeAdapter extends ArrayAdapter<Entry> {
    private final Context context;
    private final List<Entry> objects;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    private final ArrayAdapter<Inspectee> spinnerNamesAdapter;
    private final ArrayAdapter<CharSequence> adapterComments;
    private final ArrayAdapter<CharSequence> adapterAnimals;

    private final Map<String,ArrayAdapter<CharSequence>> animalsToGenres;

    void revertRegister() {
        objects.forEach(x->x.setInRegister(!x.isInRegister()));
    }


    private class SpinnerListener implements AdapterView.OnItemSelectedListener {
        private final Entry item;
        private final Spinner genreSpinner;

        SpinnerListener(Entry item, Spinner genreSpinner) {
            this.item = item;
            this.genreSpinner = genreSpinner;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            String selectedItem = (String) parent.getItemAtPosition(pos);
            item.setAnimalType(selectedItem);
            genreSpinner.setAdapter(animalsToGenres.get(item.getAnimalType()));
            genreSpinner.setOnItemSelectedListener(new SpinnerGenreListener(item));
            genreSpinner.setSelection(animalsToGenres.get(item.getAnimalType()).getPosition(item.getAnimalGenre()));
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //do Nothing
        }
    }

    private class SpinnerGenreListener implements AdapterView.OnItemSelectedListener {
        private final Entry item;

        SpinnerGenreListener(Entry item) {
            this.item = item;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            String selectedItem = (String) parent.getItemAtPosition(pos);
            item.setAnimalGenre(selectedItem);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //do Nothing
        }
    }

    private class SpinnerNamesListener implements AdapterView.OnItemSelectedListener {
        private final Entry item;

        SpinnerNamesListener(Entry item) {
            this.item = item;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            Inspectee selectedItem = (Inspectee) parent.getItemAtPosition(pos);
            item.setProducer(selectedItem.getName());
            item.setProducerTin(selectedItem.getTin());
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //do Nothing
        }
    }

    private class SpinnerCommentsListener implements AdapterView.OnItemSelectedListener {
        private final Entry item;
        private final CheckBox checkBox;

        SpinnerCommentsListener(Entry item, CheckBox checkBox) {
            this.item = item;
            this.checkBox = checkBox;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            String selectedItem = (String) parent.getItemAtPosition(pos);
            item.setComment(CommentType.fromString(selectedItem));
            if (CommentType.SINGLE != item.getComment()
                    && CommentType.IN_ORDER != item.getComment()
                    && CommentType.EMPTY != item.getComment()) {
                item.setInRegister(false);
                checkBox.setChecked(item.isInRegister());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            //do Nothing
        }
    }


    public InspectionStepThreeAdapter(Context context, int resource, List<Inspectee> producerNames, List<Entry> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        spinnerNamesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, producerNames.toArray(new Inspectee[]{}));
        spinnerNamesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterAnimals = ArrayAdapter.createFromResource(context,
                R.array.animals_array, android.R.layout.simple_spinner_item);
        adapterAnimals.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        animalsToGenres = new HashMap<>();

        ArrayAdapter<CharSequence> adapterSheepGenre = ArrayAdapter.createFromResource(context,
                R.array.sheep_genre, android.R.layout.simple_spinner_dropdown_item);
        adapterSheepGenre.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapterGoatGenre = ArrayAdapter.createFromResource(context,
                R.array.goat_genre, android.R.layout.simple_spinner_dropdown_item);
        adapterGoatGenre.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapterHorseGenre = ArrayAdapter.createFromResource(context,
                R.array.horse_genre, android.R.layout.simple_spinner_dropdown_item);
        adapterHorseGenre.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        animalsToGenres.put(context.getResources().getString(R.string.goatAnimal), adapterGoatGenre);
        animalsToGenres.put(context.getResources().getString(R.string.kidAnimal), adapterGoatGenre);
        animalsToGenres.put(context.getResources().getString(R.string.heGoatAnimal), adapterGoatGenre);

        animalsToGenres.put(context.getResources().getString(R.string.sheepAnimal), adapterSheepGenre);
        animalsToGenres.put(context.getResources().getString(R.string.lambAnimal), adapterSheepGenre);
        animalsToGenres.put(context.getResources().getString(R.string.ramAnimal), adapterSheepGenre);

        animalsToGenres.put(context.getResources().getString(R.string.horseAnimal), adapterHorseGenre);

        adapterComments = ArrayAdapter.createFromResource(context, R.array.comments_array,android.R.layout.simple_spinner_item);
        adapterComments.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.edititems, parent, false);
        }
        final Entry item = objects.get(position);

        TextView textView = rowView.findViewById(R.id.textView);
        TextView tagDateView = rowView.findViewById(R.id.editText2);

        textView.setOnClickListener(view -> {
            EditTagDialog editListItemDialog = new EditTagDialog(getContext(), item, objects);
            editListItemDialog.show();
        });

        textView.setOnLongClickListener(view -> {
            if (!item.isAutomatic()) {
                AlertDialog.Builder preDeleteBuilder = new AlertDialog.Builder(view.getContext());
                preDeleteBuilder.setTitle("Διαγραφή ενωτίου;")
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                            remove(item);
                            notifyDataSetChanged();
                        })
                        .setNegativeButton(android.R.string.no, (dialog, which) -> {
                            // do nothing
                        });
                preDeleteBuilder.show();
            }
            return true;
        });
        final CheckBox checkBox = rowView.findViewById(R.id.checkBox);

        Spinner spinner = rowView.findViewById(R.id.spinner);
        Spinner genreSpinner = rowView.findViewById(R.id.biospinner);

        spinner.setAdapter(adapterAnimals);


        spinner.setOnItemSelectedListener(new SpinnerListener(item, genreSpinner));

        spinner.setSelection(adapterAnimals.getPosition(item.getAnimalType()));

        genreSpinner.setAdapter(animalsToGenres.get(item.getAnimalType()));
        genreSpinner.setOnItemSelectedListener(new SpinnerGenreListener(item));
        genreSpinner.setSelection(animalsToGenres.get(item.getAnimalType()).getPosition(item.getAnimalGenre()));

        Spinner spinnerNames = rowView.findViewById(R.id.spinnerNames);
        spinnerNames.setAdapter(spinnerNamesAdapter);
        spinnerNames.setOnItemSelectedListener(new SpinnerNamesListener(item));
        spinnerNames.setOnLongClickListener(view -> {
            Inspectee inspectee = (Inspectee) spinnerNames.getSelectedItem();
            item.getOwner();
            objects.stream().filter(x -> x.getOwner().equals(item.getOwner())).forEach(x ->
            {
                x.setProducer(inspectee.getName());
                x.setProducerTin(inspectee.getTin());
            });
            notifyDataSetChanged();
            return true;
        });
        spinnerNames.setSelection(spinnerNamesAdapter.getPosition(new Inspectee(item.getProducerTin(), item.getProducer())));

        Spinner spinnerComments = rowView.findViewById(R.id.viewComments);
        spinnerComments.setAdapter(adapterComments);
        spinnerComments.setOnItemSelectedListener(new SpinnerCommentsListener(item,checkBox));
        spinnerComments.setSelection(adapterComments.getPosition(item.getComment().getTitle()));

        textView.setText(TagFormatter.format(item.getTag()));
        tagDateView.setText(simpleDateFormat.format(item.getTagDate()));
        checkBox.setChecked(item.isInRegister());

        checkBox.setOnClickListener(view -> {
            item.setInRegister(!item.isInRegister());
            checkBox.setChecked(item.isInRegister());
            if (item.isInRegister() &&
                    item.getComment() != CommentType.EMPTY
                    && item.getComment() != CommentType.IN_ORDER
                    && item.getComment() != CommentType.SINGLE) {
                item.setComment(CommentType.EMPTY);
                spinnerComments.setSelection(adapterComments.getPosition(item.getComment().getTitle()));

            }
        });
        return rowView;
    }

}
