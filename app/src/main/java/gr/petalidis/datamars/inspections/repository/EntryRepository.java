/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.slf4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.CommentType;
import gr.petalidis.datamars.inspections.domain.Entry;
import gr.petalidis.datamars.rsglibrary.Rsg;

public class EntryRepository {
    private static final Logger log = Log4jHelper.getLogger(EntryRepository.class.getName());

    private EntryRepository() {
    }

    private static long insertEntry(SQLiteDatabase db, Entry e) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        ContentValues values = new ContentValues();
        values.put("id", e.getId().toString());
        values.put("inspectionId", e.getInspectionId().toString());
        values.put("tag", e.getTag());
        values.put("country", e.getCountry());
        values.put("tagDate", dateFormat.format(e.getTagDate()));
        values.put("animalType", e.getAnimalType());
        values.put("animalGenre", e.getAnimalGenre());
        values.put("producer", e.getProducer());
        values.put("producerTin", e.getProducerTin());
        values.put("isInRegister", e.isInRegister() ? 1 : 0);
        values.put("comment", e.getComment().getTitle());
        values.put("isAutomatic", e.isAutomatic() ? 1 : 0);

        long insert = db.insert(DbHandler.TABLE_INSPECTION_ENTRIES, null, values);
        if (insert == -1) {
            log.error("Got insertion error for producer with Tin {}", insert, e.getProducerTin());
        }
        return insert;
    }

    public static void save(DbHandler dbHandler, List<Entry> entries) {

        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            entries.forEach(e -> insertEntry(db, e));


        }
    }

    public static List<Rsg> getAlreadyCheckedRsgsFor(DbHandler dbHandler, Date inspectionDate) throws ParseException {
        List<Rsg> inspectedRsgs = new ArrayList<>();
        SimpleDateFormat onlyDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat fullDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        String realInspectionDate = onlyDateFormat.format(inspectionDate) + "%";

        String[] columns = {"tag", "country", "tagDate"};
        String selection = "tagDate like ? AND producerTin!='000000000'";
        String[] selectionArgs = {realInspectionDate};

        //The real query is as follows
        //        SELECT tag, country, tagDate FROM  DbHandler.TABLE_INSPECTION_ENTRIES  where tagDate like realInspectionDate AND producerTin!='000000000';
        try (final SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTION_ENTRIES, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.moveToFirst()) {
                do {
                    Rsg entry = new Rsg(cursor.getString(1),
                            cursor.getString(0),
                            fullDateFormat.parse(cursor.getString(2)));
                    inspectedRsgs.add(entry);
                } while (cursor.moveToNext());
            }
        }
        return inspectedRsgs;
    }

    static List<Entry> getEntriesFor(DbHandler dbHandler, UUID inspectionId) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        List<Entry> entries = new ArrayList<>();

        String[] columns = {"id", "inspectionId", "tag", "country", "tagDate", "animalType",
                "animalGenre", "producer", "producerTin", "isInRegister", "comment", "isAutomatic"};
        String selection = "inspectionId = ?";
        String[] selectionArgs = {inspectionId.toString()};

        try (final SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTION_ENTRIES, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.moveToFirst()) {
                do {
                    Entry entry = new Entry();
                    entry.setId(UUID.fromString(cursor.getString(0)));
                    entry.setInspectionId(UUID.fromString(cursor.getString(1)));
                    entry.setTag(cursor.getString(2));
                    entry.setCountry(cursor.getString(3));
                    entry.setTagDate(dateFormat.parse(cursor.getString(4)));
                    entry.setAnimalType(cursor.getString(5));
                    entry.setAnimalGenre(cursor.getString(6));
                    entry.setProducer(cursor.getString(7));
                    entry.setProducerTin(cursor.getString(8));
                    entry.setInRegister(cursor.getInt(9) == 1);
                    entry.setComment(CommentType.fromString(cursor.getString(10)));
                    entry.setAutomatic(cursor.getInt(11) == 1);

                    entries.add(entry);
                } while (cursor.moveToNext());
            }
        }
        return entries;
    }


    public static void update(DbHandler dbHandler, List<Entry> entries) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            entries.forEach(e -> {
                ContentValues values = new ContentValues();
                String[] selectionArgs = {e.getId().toString()};
                values.put("inspectionId", e.getInspectionId().toString());
                values.put("tag", e.getTag());
                values.put("country", e.getCountry());
                values.put("tagDate", dateFormat.format(e.getTagDate()));
                values.put("animalType", e.getAnimalType());
                values.put("animalGenre", e.getAnimalGenre());
                values.put("producer", e.getProducer());
                values.put("producerTin", e.getProducerTin());
                values.put("isInRegister", e.isInRegister() ? 1 : 0);
                values.put("comment", e.getComment().getTitle());

                int update = db.update(DbHandler.TABLE_INSPECTION_ENTRIES, values, "id = ?", selectionArgs);
                if (update == 0) {
                    insertEntry(db, e);
                }
            });


        }
    }

    public static void deleteWithInspectionId(DbHandler dbHandler, UUID inspectionId) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            String[] selectionArgs = {inspectionId.toString()};
            int deleted = db.delete(DbHandler.TABLE_INSPECTION_ENTRIES, "inspectionId = ?", selectionArgs);
            log.info("Deleted {} number of entries with inspection id {}", deleted, inspectionId);
        }
    }

    public static void deleteInvalidEntriesFromInspection(DbHandler dbHandler, UUID id, List<Entry> invalidEntries) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            for (Entry entry : invalidEntries) {
                String[] selectionArgs = {id.toString(), entry.getId().toString()};
                int deleted = db.delete(DbHandler.TABLE_INSPECTION_ENTRIES, "inspectionId = ? and id = ?", selectionArgs);
                log.info("Deleted {} number of entries with inspection id {} and id {}", deleted, id, entry.getId());
            }
        }
    }

}
