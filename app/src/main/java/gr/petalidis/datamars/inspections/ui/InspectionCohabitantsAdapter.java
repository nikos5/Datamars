/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.Inspection;

class InspectionCohabitantsAdapter extends ArrayAdapter<Inspectee> {
    private final Context context;
    private final Inspection inspection;
    private final List<Inspectee> inspectees;
    private static final String INSPECTION_KEY = "inspection";
    private static final String INSPECTEE_KEY = "inspectee";


    public InspectionCohabitantsAdapter(Context context, int resource, Inspection inspection, List<Inspectee> inspectees) {
        super(context, resource, inspectees);
        this.context = context;
        this.inspection = inspection;
        this.inspectees = inspectees;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.inspection_cohabitants_list, parent, false);
        }
        final Inspectee item = inspectees.get(position);


        TextView inspectionProducer = rowView.findViewById(R.id.inspectionProducer);
        inspectionProducer.setText(item.getName());

        TextView inspectionProducerTin = rowView.findViewById(R.id.inspectionProducerTin);
        inspectionProducerTin.setText(item.getTin());

        ImageButton proceedButton = rowView.findViewById(R.id.proceedToCohabitantButton);

        proceedButton.setOnClickListener(view -> {
            Intent intent = new Intent(context, InspectionStepFourActivity.class);

            intent.putExtra(INSPECTION_KEY, inspection);
            intent.putExtra(INSPECTEE_KEY, item);

            context.startActivity(intent);
        });
        ImageButton infoButton = rowView.findViewById(R.id.infoCohabitant);
        infoButton.setOnClickListener(view -> {
            AlertDialog.Builder preSaveBuilder = new AlertDialog.Builder(context);

            String totalResult = inspection.generateReportFor(item).toHtmlString();

            WebView webView = new WebView(context);
            webView.setTag(R.id.WEBVIEW, "InfoView");
            webView.setVerticalScrollBarEnabled(true);
            webView.loadData(totalResult, "text/html", "utf-8");
            preSaveBuilder.setTitle("Πληροφορίες ελέγχου " + item.toString()).setView(webView)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        // do nothing
                    })
            ;
            preSaveBuilder.show();
        });

        return rowView;
    }

}
