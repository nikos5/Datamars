/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.slf4j.Logger;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.activities.StartActivity;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.repository.EntryRepository;
import gr.petalidis.datamars.inspections.validators.TinValidator;
import gr.petalidis.datamars.rsglibrary.Rsg;
import gr.petalidis.datamars.rsglibrary.RsgReader;

public class InspectionStepTwoActivity extends AppCompatActivity {
    private static final String INSPECTION_DATE = "inspectionDate";
    private static final String RSG_FILENAME = "rsgFilename";
    private static final String INSPECTION_KEY = "inspection";

    private Inspection inspection;

    private static final Logger log = Log4jHelper.getLogger(InspectionStepTwoActivity.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inspection_step_two);

        restore(savedInstanceState == null ? getIntent().getExtras() : savedInstanceState);

        setProducer1Name();
        setProducer2Name();
        setProducer3Name();
        setProducer4Name();
        setProducer1Tin();
        setProducer2Tin();
        setProducer3Tin();
        setProducer4Tin();

        EditText tin = findViewById(R.id.producer1TinText);
        tin.addTextChangedListener(new TinWatcher(new TinValidator(), tin));
        tin = findViewById(R.id.producer2TinText);
        tin.addTextChangedListener(new TinWatcher(new TinValidator(), tin));
        tin = findViewById(R.id.producer3TinText);
        tin.addTextChangedListener(new TinWatcher(new TinValidator(), tin));
        tin = findViewById(R.id.producer4TinText);
        tin.addTextChangedListener(new TinWatcher(new TinValidator(), tin));

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }

    private void restore(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            log.error("Attempt to create Activity with no date. Saved instances was null");
            return;
        }

        boolean createNewInspection = savedInstanceState.getSerializable(INSPECTION_KEY) == null;

        if (!createNewInspection && !(savedInstanceState.getSerializable(INSPECTION_KEY) instanceof Inspection)) {
            log.error("Could not retrieve inspection");
            return;
        }

        if (createNewInspection) {
            if (!(savedInstanceState.getSerializable(INSPECTION_DATE) instanceof Date)) {
                log.error("Could not retrieve inspection date");
                return;
            }

            Date inspectionDate = (Date) savedInstanceState.getSerializable(INSPECTION_DATE);
            String rsgFilename = savedInstanceState.getString(RSG_FILENAME);

            createNewInspection(rsgFilename, inspectionDate);
        } else {
            inspection = (Inspection) savedInstanceState.getSerializable(INSPECTION_KEY);
            setUpSpinners(this, inspection);
        }

    }

    private void createNewInspection(String filename, Date inspectionDate) {

        InspectionLoadRsgAsyncTask inspectionLoadRsgAsyncTask = new InspectionLoadRsgAsyncTask(this);
        inspectionLoadRsgAsyncTask.execute(filename, new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(inspectionDate));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        menu.removeItem(R.id.temp_save);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.about) {
            PackageManager packageManager = this.getPackageManager();
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                        //Do Nothing
                    });
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                        "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                        "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                builder.show();
            } catch (PackageManager.NameNotFoundException e) {
                log.error("Could not show about message {}", e.getLocalizedMessage());
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INSPECTION_KEY, inspection);
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    private boolean formHasErrors() {
        boolean hasErrors = false;
        TinValidator validator = new TinValidator();
        EditText tin = findViewById(R.id.producer1TinText);
        String tinString = tin.getText().toString();
        EditText producer1Name = findViewById(R.id.producer1NameText);
        String producer1NameString = producer1Name.getText().toString();

        EditText tin2 = findViewById(R.id.producer2TinText);
        String tinString2 = tin2.getText().toString();


        EditText tin3 = findViewById(R.id.producer3TinText);
        String tinString3 = tin3.getText().toString();

        EditText tin4 = findViewById(R.id.producer4TinText);
        String tinString4 = tin4.getText().toString();

        if (!validator.isValid(tinString)) {
            hasErrors = true;
            tin.setError(this.getApplicationContext().getString(R.string.tinIncorrect));
        }
        if (producer1NameString.length() < 2) {
            hasErrors = true;
            producer1Name.setError("Υποχρεωτικό πεδίο");
        }

        if (tinString2.length() > 0 && !validator.isValid(tinString2)) {
            hasErrors = true;
            tin2.setError(this.getApplicationContext().getString(R.string.tinIncorrect));
        }

        if (tinString3.length() > 0 && !validator.isValid(tinString2)) {
            hasErrors = true;
            tin3.setError(this.getApplicationContext().getString(R.string.tinIncorrect));
        }

        if (tinString4.length() > 0 && !validator.isValid(tinString2)) {
            hasErrors = true;
            tin4.setError(this.getApplicationContext().getString(R.string.tinIncorrect));
        }

        return hasErrors;

    }

    public void goToInspectionStepThreeActivity(View view) {

        if (!formHasErrors() && !inspection.getEntries().isEmpty()) {
            Spinner producer1TagSpinner = findViewById(R.id.producer1TagValue);
            String producer1Tag = (String) producer1TagSpinner.getSelectedItem();

            Spinner producer2TagSpinner = findViewById(R.id.producer2TagValue);
            String producer2Tag = (String) producer2TagSpinner.getSelectedItem();
            Spinner producer3TagSpinner = findViewById(R.id.producer3TagValue);
            String producer3Tag = (String) producer3TagSpinner.getSelectedItem();
            Spinner producer4TagSpinner = findViewById(R.id.producer4TagValue);
            String producer4Tag = (String) producer4TagSpinner.getSelectedItem();

            Spinner animalTypeSpinner = findViewById(R.id.animalType);
            String animalType = (String) animalTypeSpinner.getSelectedItem();

            Intent intent = new Intent(this, InspectionStepThreeActivity.class);

            inspection.setProducer1Tin(getProducer1Tin());
            inspection.setProducer1Name(getProducer1Name());

            inspection.setProducer2Name(getProducer2Name());
            inspection.setProducer2Tin(getProducer2Tin());

            inspection.setProducer3Name(getProducer3Name());
            inspection.setProducer3Tin(getProducer3Tin());

            inspection.setProducer4Name(getProducer4Name());
            inspection.setProducer4Tin(getProducer4Tin());


            if (inspection.hasUnassignedEntries()) {
                inspection.getEntries()
                        .forEach(x -> {
                            x.setProducerTin(inspection.getProducer1Tin());
                            x.setProducer(inspection.getProducer1Name());
                            x.setAnimalType(animalType);
                        });
            }

            if (!inspection.hasOwnerTag(producer1Tag, inspection.getProducer1Tin())) {
                inspection.getEntries().stream().filter(x -> x.getOwner().equals(producer1Tag)).forEach(x -> {
                    x.setProducerTin(inspection.getProducer1Tin());
                    x.setProducer(inspection.getProducer1Name());
                    x.setAnimalType(animalType);
                });
            }

            if (!inspection.getProducer2Tin().isEmpty() &&
                    (inspection.getEntriesFor(inspection.getProducer2Tin()).isEmpty()
                            || !inspection.hasOwnerTag(producer2Tag, inspection.getProducer2Tin()))) {
                inspection.getEntries().stream().filter(x -> x.getOwner().equals(producer2Tag)).forEach(x -> {
                    x.setProducerTin(inspection.getProducer2Tin());
                    x.setProducer(inspection.getProducer2Name());
                    x.setAnimalType(animalType);
                });
            }

            if (!inspection.getProducer3Tin().isEmpty() &&
                    (inspection.getEntriesFor(inspection.getProducer3Tin()).isEmpty()
                            || !inspection.hasOwnerTag(producer3Tag, inspection.getProducer3Tin()))) {
                inspection.getEntries().stream().filter(x -> x.getOwner().equals(producer3Tag)).forEach(x -> {
                    x.setProducerTin(inspection.getProducer3Tin());
                    x.setProducer(inspection.getProducer3Name());
                    x.setAnimalType(animalType);
                });
            }

            if (!inspection.getProducer4Tin().isEmpty() && (
                    inspection.getEntriesFor(inspection.getProducer4Tin()).isEmpty()
                            || !inspection.hasOwnerTag(producer4Tag, inspection.getProducer4Tin()))) {
                inspection.getEntries().stream().filter(x -> x.getOwner().equals(producer4Tag)).forEach(x -> {
                    x.setProducerTin(inspection.getProducer4Tin());
                    x.setProducer(inspection.getProducer4Name());
                    x.setAnimalType(animalType);
                });
            }
            intent.putExtra(INSPECTION_KEY, inspection);

            startActivity(intent);
            finish();
        }


    }


    private String getProducer1Name() {
        EditText name = findViewById(R.id.producer1NameText);
        return name.getText().toString().trim();
    }

    private String getProducer1Tin() {
        EditText tin = findViewById(R.id.producer1TinText);
        return tin.getText().toString().trim();
    }

    private String getProducer2Name() {
        EditText name = findViewById(R.id.producer2NameText);
        return name.getText().toString().trim();
    }

    private String getProducer2Tin() {
        EditText tin = findViewById(R.id.producer2TinText);
        return tin.getText().toString().trim();
    }

    private String getProducer3Name() {
        EditText name = findViewById(R.id.producer3NameText);
        return name.getText().toString().trim();
    }

    private String getProducer3Tin() {
        EditText tin = findViewById(R.id.producer3TinText);
        return tin.getText().toString().trim();
    }

    private String getProducer4Name() {
        EditText name = findViewById(R.id.producer4NameText);
        return name.getText().toString().trim();
    }

    private String getProducer4Tin() {
        EditText tin = findViewById(R.id.producer4TinText);
        return tin.getText().toString().trim();
    }

    private void setProducer1Name() {
        EditText name = findViewById(R.id.producer1NameText);
        if (inspection != null) name.setText(inspection.getProducer1Name());
    }

    private void setProducer1Tin() {
        EditText tin = findViewById(R.id.producer1TinText);
        if (inspection != null) tin.setText(inspection.getProducer1Tin());
    }

    private void setProducer2Name() {
        EditText name = findViewById(R.id.producer2NameText);
        if (inspection != null) name.setText(inspection.getProducer2Name());
    }

    private void setProducer2Tin() {
        EditText tin = findViewById(R.id.producer2TinText);
        if (inspection != null) tin.setText(inspection.getProducer2Tin());
    }

    private void setProducer3Name() {
        EditText name = findViewById(R.id.producer3NameText);
        if (inspection != null) name.setText(inspection.getProducer3Name());
    }

    private void setProducer3Tin() {
        EditText tin = findViewById(R.id.producer3TinText);
        if (inspection != null) tin.setText(inspection.getProducer3Tin());
    }

    private void setProducer4Name() {
        EditText name = findViewById(R.id.producer4NameText);
        if (inspection != null) name.setText(inspection.getProducer4Name());
    }

    private void setProducer4Tin() {
        EditText tin = findViewById(R.id.producer4TinText);
        if (inspection != null) tin.setText(inspection.getProducer4Tin());
    }


    private static class InspectionLoadRsgAsyncTask extends AsyncTask<String, String, Set<Rsg>> {

        final WeakReference<InspectionStepTwoActivity> parent;

        InspectionLoadRsgAsyncTask(InspectionStepTwoActivity parent) {
            this.parent = new WeakReference<>(parent);
        }

        private final Logger log = Log4jHelper.getLogger(InspectionLoadRsgAsyncTask.class.getName());

        protected Set<Rsg> doInBackground(String... strings) {

            int count = strings.length;

            if (count != 2) {
                return new HashSet<>();
            }
            try {

                String filename = strings[0];
                Date date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(strings[1]);
                DbHandler dbHandler = new DbHandler(Moo.getAppContext());

                List<Rsg> alreadyCheckedRsgs = EntryRepository.getAlreadyCheckedRsgsFor(dbHandler, date);

                Set<Rsg> rsgs = RsgReader.readRsgFromTablet(filename)
                        .stream()
                        .filter(rsg -> alreadyCheckedRsgs.stream().noneMatch(seenRsg -> seenRsg.equals(rsg)))
                        .collect(Collectors.toSet());
                log.trace("Finished reading rsgs");
                return rsgs;

            } catch (IOException | ParseException e) {
                log.error("Received exception: {}", e.getLocalizedMessage());
                return new HashSet<>();

            }
        }

        @Override
        protected void onPostExecute(Set<Rsg> rsgs) {
            if (parent.get() == null) {
                log.error("We do not have a parent");
                return;
            }
            if (rsgs.isEmpty()) {
                Notifier.notify(parent.get(), "Δεν υπάρχουν σκαναρισμένα ενώτια για τον έλεγχο αυτό!", Notifier.NOTIFICATION_MESSAGE_TYPE.INFO_MESSAGE);
            } else {
                rsgs.stream().findAny().ifPresent(rsg -> {
                    InspectionStepTwoActivity activity = parent.get();
                    if (activity == null || activity.isFinishing()) {
                        log.error("Activity is {} ", (activity == null || activity.isFinishing() ? "Finishing or null" : " not finishing"));
                        return;
                    }
                    activity.inspection = new Inspection(rsg.getDate(), "000000000", "");

                    activity.inspection.initEntries(rsgs);
                    setUpSpinners(activity, activity.inspection);

                });

            }

        }

    }

    private static void setUpSpinners(InspectionStepTwoActivity activity, Inspection theInspection) {
        String[] owners = theInspection.getOwnerTags().toArray(new String[]{});

        Spinner producer1TagSpinner = activity.findViewById(R.id.producer1TagValue);
        producer1TagSpinner.setAdapter(new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, owners));

        Spinner producer2TagSpinner = activity.findViewById(R.id.producer2TagValue);
        ArrayAdapter<String> producer2Adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, owners);
        producer2TagSpinner.setAdapter(producer2Adapter);
        if (theInspection.hasProducer2()) {
            producer2TagSpinner.setSelection(producer2Adapter.getPosition(theInspection.getMostFrequentOwnerTagFor(theInspection.getProducer2Tin())));
        }

        Spinner producer3TagSpinner = activity.findViewById(R.id.producer3TagValue);
        ArrayAdapter<String> producer3Adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, owners);
        if (theInspection.hasProducer3()) {
            producer3TagSpinner.setSelection(producer3Adapter.getPosition(theInspection.getMostFrequentOwnerTagFor(theInspection.getProducer3Tin())));
        }
        producer3TagSpinner.setAdapter(producer3Adapter);

        Spinner producer4TagSpinner = activity.findViewById(R.id.producer4TagValue);
        ArrayAdapter<String> producer4Adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, owners);
        if (theInspection.hasProducer4()) {
            producer4TagSpinner.setSelection(producer4Adapter.getPosition(theInspection.getMostFrequentOwnerTagFor(theInspection.getProducer4Tin())));
        }
        producer4TagSpinner.setAdapter(producer4Adapter);
        TextView animalCountValue = activity.findViewById(R.id.animalCountValue);
        animalCountValue.setText(theInspection.getEntries().size() + "");
    }

}