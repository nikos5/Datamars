/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.ScannedDocument;

public class ScannedDocumentRepository {
    private ScannedDocumentRepository() {
    }

    private static final Logger log = Log4jHelper.getLogger(ScannedDocumentRepository.class.getName());

    public static Set<String> getAllScannedNames(DbHandler dbHandler) {
        Set<String> scannedDocuments = new HashSet<>();

        final String[] columns = {
                "imagePath"
        };

        final String selection = "";
        final String[] selectionArgs = {};

        try (final SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTION_SCANNED_DOCUMENT, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.moveToFirst()) {
                do {
                    scannedDocuments.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        }
        return scannedDocuments;
    }

    static List<ScannedDocument> getScannedDocumentFor(DbHandler dbHandler, UUID inspectionId) {
        List<ScannedDocument> scannedDocuments = new ArrayList<>();
        final String[] columns = {
                "id",
                "inspectionId",
                "imagePath"};
        final String selection = "inspectionId = ?";
        final String[] selectionArgs = {inspectionId.toString()};

        try (final SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTION_SCANNED_DOCUMENT, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.moveToFirst()) {
                do {
                    ScannedDocument scannedDocument = new ScannedDocument(UUID.fromString(cursor.getString(1)),cursor.getString(2));
                    scannedDocument.setId(UUID.fromString(cursor.getString(0)));
                    scannedDocuments.add(scannedDocument);
                } while (cursor.moveToNext());
            }
        }
        return scannedDocuments;
    }

    public static void save(DbHandler dbHandler, List<ScannedDocument> scannedDocuments) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            scannedDocuments.forEach(e -> {
                ContentValues values = new ContentValues();
                values.put("id", e.getId().toString());
                values.put("inspectionId", e.getInspectionId().toString());
                values.put("imagePath", e.getImagePath());

                db.insert(DbHandler.TABLE_INSPECTION_SCANNED_DOCUMENT, null, values);
            });

        }
    }

    public static void update(DbHandler dbHandler, List<ScannedDocument> scannedDocuments) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            scannedDocuments.forEach(e -> {
                ContentValues values = new ContentValues();
                String[] selectionArgs = {e.getId().toString()};
                values.put("inspectionId", e.getInspectionId().toString());
                values.put("imagePath", e.getImagePath());

                db.update(DbHandler.TABLE_INSPECTION_SCANNED_DOCUMENT, values, "id = ?", selectionArgs);
            });

        }
    }

    public static void deleteWithInspectionId(DbHandler dbHandler, UUID inspectionId) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            String[] selectionArgs = {inspectionId.toString()};
            int deleted = db.delete(DbHandler.TABLE_INSPECTION_SCANNED_DOCUMENT, "inspectionId = ?", selectionArgs);
            log.info("Deleted {} number of entries with inspection id {}", deleted, inspectionId);
        }
    }
}
