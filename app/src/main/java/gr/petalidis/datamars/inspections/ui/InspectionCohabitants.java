/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.slf4j.Logger;

import java.util.Locale;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.activities.StartActivity;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;
import gr.petalidis.datamars.inspections.utilities.WGS84Converter;

public class InspectionCohabitants extends AppCompatActivity {
    private static final int PERMISSION_GPS_REQUEST_CODE = 2;

    private static final String INSPECTION_KEY = "inspection";
    private static final Logger log = Log4jHelper.getLogger(InspectionCohabitants.class.getName());
    private Inspection inspection;
    private Context mContext;


    private Location gpsLocation = null;

    private boolean requestLocationUpdates = true;

    private final LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            for (Location location : locationResult.getLocations()) {
                setGpsLocation(location);
            }
        }
    };

    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.mContext = this;
        // recovering the instance state
        if (savedInstanceState != null) {
            inspection = (Inspection) savedInstanceState.getSerializable(INSPECTION_KEY);
            requestLocationUpdates = savedInstanceState.getBoolean("requestLocationUpdates");
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable(INSPECTION_KEY);
        }
        setContentView(R.layout.inspection_cohabitants);


        gpsToggle(null);

        final ListView listview = findViewById(R.id.inspectionCohabitantsList);

        final InspectionCohabitantsAdapter adapter = new InspectionCohabitantsAdapter(this,
                android.R.layout.simple_list_item_1, inspection, inspection.getProducersWithNoDummy());

        listview.setAdapter(adapter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // recovering the instance state
        checkLocationPermission();


    }

    public void gpsToggle(View view) {
        ImageButton button = findViewById(R.id.gpsButton);
        requestLocationUpdates = !requestLocationUpdates;

        if (!requestLocationUpdates) {
            stopLocationUpdates();
            Drawable locationOff = getResources().getDrawable(R.drawable.ic_baseline_location_off_48, null);
            button.setImageDrawable(locationOff);
        } else {
            checkLocationPermission();
            Drawable locationOn = getResources().getDrawable(R.drawable.ic_outline_location_on_48, null);
            button.setImageDrawable(locationOn);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INSPECTION_KEY, inspection);
        outState.putBoolean("requestLocationUpdates", requestLocationUpdates);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.temp_save:
                if (inspection != null) {
                    DbHandler dbHandler = new DbHandler(Moo.getAppContext());
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                                //Do Nothing
                            });
                    try {
                        double[] coordinates = {0.0, 0.0};
                        if (gpsLocation != null) {
                            coordinates = WGS84Converter.toGGRS87(gpsLocation.getLatitude(), gpsLocation.getLongitude());
                        }
                        inspection.setLatitude(coordinates[0]);
                        inspection.setLongitude(coordinates[1]);

                        inspection = InspectionService.save(dbHandler, inspection);

                        builder.setTitle(R.string.success).setMessage(R.string.success);
                        builder.show();
                    } catch (PersistenceException e) {
                        log.error("Αποτυχία αποθήκευσης ελέγχου {}. Λόγος {}", inspection.getProducer1Tin(), e.getMessage());
                        builder.setTitle(R.string.failure).setMessage(e.getMessage());
                        builder.show();
                    }
                }
                return true;
            case R.id.about:
                PackageManager packageManager = this.getPackageManager();
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                            //Do Nothing
                        });
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                    String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                            "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                            "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                    builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                    builder.show();
                } catch (PackageManager.NameNotFoundException e) {
                    log.error("Could not show about message {}", e.getLocalizedMessage());
                }

                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void save(View view) {
        AlertDialog.Builder preSaveBuilder = new AlertDialog.Builder(this);

        String totalResult = inspection.getProducersWithNoDummy().stream().map(
                inspectee -> inspection.generateReportFor(inspectee).toHtmlString()
        ).collect(Collectors.joining("<br/>\n"));

        String msg = totalResult + "<br/><span id=\"webviewEnd\">Να γίνει αποθήκευση;</span>";
        WebView webView = new WebView(this.getApplicationContext());
        webView.setTag(R.id.WEBVIEW, "WebView");
        WebSettings webSettings = webView.getSettings();
        webView.setVerticalScrollBarEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(msg, "text/html", "utf-8");
        preSaveBuilder.setTitle("Αποθήκευση ελέγχου").setView(webView)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    double[] coordinates = {0.0, 0.0};
                    if (gpsLocation != null) {
                        coordinates = WGS84Converter.toGGRS87(gpsLocation.getLatitude(), gpsLocation.getLongitude());
                    }
                    inspection.setLatitude(coordinates[0]);
                    inspection.setLongitude(coordinates[1]);
                    DbHandler dbHandler = new DbHandler(Moo.getAppContext());

                    try {
                        InspectionService.finaliseInspections(dbHandler, inspection);
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(R.string.success).setMessage(R.string.success);
                        builder.show();
                    } catch (PersistenceException e) {
                        log.error("Αποτυχία αποθήκευσης ελέγχου {}. Λόγος {}", inspection.getProducer1Tin(), e.getMessage());
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(R.string.failure).setMessage(e.getMessage());
                        builder.show();
                    }
                    Intent intent = new Intent(mContext, StartActivity.class);
                    startActivity(intent);
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    // do nothing
                });
        AlertDialog show = preSaveBuilder.show();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        int min = width > height ? height : width;
        show.getWindow().setLayout((int) (0.9 * min), (int) (0.9 * min));


    }

    private void setGpsLocation(Location location) {

        if (location != null &&
                (gpsLocation == null || location.distanceTo(gpsLocation) > 1 || location.getAccuracy() < gpsLocation.getAccuracy())) {
            gpsLocation = location;

            double[] coordinates;

            coordinates = WGS84Converter.toGGRS87(gpsLocation.getLatitude(), gpsLocation.getLongitude());

            TextView gpsLocationView = findViewById(R.id.gpsValue);
            float accuracy = location.getAccuracy();
            gpsLocationView.setText(String.format(Locale.forLanguageTag("el"), "%.2f", coordinates[0])
                    + ", " + String.format(Locale.forLanguageTag("el"), "%.2f", coordinates[1]) + "\n (ακρίβεια 68% εντός:"
                    + String.format(Locale.forLanguageTag("el"), "%.2f", accuracy) + "μ)");
        }
    }

    private void checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // TODO: Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Αίτημα χρήσης υπηρεσιών τοποθεσίας")
                        .setMessage("Η τοποθεσία χρησιμοποιείται για την τοποθέτηση του στάβλου όταν καταχωρείτε έλεγχο")
                        .setPositiveButton(android.R.string.ok, (dialogInterface, i) ->
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_GPS_REQUEST_CODE))
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_GPS_REQUEST_CODE);
            }
        } else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, this::setGpsLocation);
            if (requestLocationUpdates) {
                LocationRequest currentLocationRequest = new LocationRequest();
                currentLocationRequest.setInterval(5000)
                        .setFastestInterval(0)
                        .setMaxWaitTime(0)
                        .setSmallestDisplacement(0)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                if (mFusedLocationClient != null) {
                    mFusedLocationClient.requestLocationUpdates(currentLocationRequest, mLocationCallback, null);
                }
            } else {
                stopLocationUpdates();
            }
        }
    }

    private void showGpsFailure() {
        AlertDialog.Builder gpsAlertDialog = new AlertDialog.Builder(this);
        String msg = "Δεν ήταν δυνατή η ανάγνωση της τοποθεσίας. Δοκιμάσετε αργότερα";
        gpsAlertDialog.setTitle("Αδυναμία πρόσβασης στις υπηρεσίες τοποθεσίας").setMessage(msg)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {

                });
        gpsAlertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GPS_REQUEST_CODE) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                showGpsFailure();
            } else {
                checkLocationPermission();
            }
        }
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkLocationPermission();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InspectionStepThreeActivity.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        startActivity(intent);
    }
}
