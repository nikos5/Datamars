/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui.login;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.ArrayList;

import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.service.UploadInspectionsService;
import gr.petalidis.datamars.inspections.service.UploadResult;
import gr.petalidis.datamars.inspections.service.UploadResultReceiver;

public class LoginViewModel extends AndroidViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<UploadResult> loginResult = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<UploadResult> getLoginResult() {
        return loginResult;
    }

    public void login(String username, String username2, String password, ArrayList<String> idList, ProgressBar progressBar) {
        // can be launched in a separate asynchronous job
        UploadResultReceiver uploadResultReceiver =
                new UploadResultReceiver(getApplication().getApplicationContext(), new Handler(getApplication().getApplicationContext().getMainLooper()), progressBar);

        Observer<UploadResult> observer = new Observer<UploadResult>() {
            @Override
            public void onChanged(@Nullable UploadResult uploadResult) {
                if (uploadResult == null) {
                    return;
                }
                loginResult.setValue(uploadResult);
                uploadResultReceiver.getResult().removeObserver(this);
            }
        };
        uploadResultReceiver.getResult().observeForever(observer);
        Intent cbIntent = new Intent();
        cbIntent.setClass(getApplication().getApplicationContext(), UploadInspectionsService.class);
        cbIntent.putExtra("receiver", uploadResultReceiver);
        cbIntent.putExtra("username1", username);
        cbIntent.putExtra("username2", username2);
        cbIntent.putExtra("password", password);
        cbIntent.putStringArrayListExtra("inspections", idList);

        getApplication().startService(cbIntent);
//        Result<UserDto> result =new Result.Success<>(nll);
//
//        if (result instanceof Result.Success) {
//            LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
//            loginResult.setValue(new LoginResult(new LoggedInUserView(data.getDisplayName())));
//        } else {
//            loginResult.setValue(new LoginResult(R.string.login_failed));
//        }
    }

    public void loginDataChanged(String username, String username2, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, null, R.string.invalid_password));
        } else if (!isUserNameValid(username2)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_username, null));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        return !username.trim().isEmpty();
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}