/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import org.slf4j.Logger;

import java.io.IOException;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.inspections.dto.LoginDto;
import gr.petalidis.datamars.inspections.dto.UserDto;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class LoginApiClient {
    static final String BASE_URL = Moo.getProperty("server");
    private static final Logger log = Log4jHelper.getLogger(LoginApiClient.class.getName());

    public static UserDto login(LoginDto loginDto) throws IOException {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();

        LoginApi service = retrofit.create(LoginApi.class);

        Call<UserDto> callSync = service.login(loginDto);

        Response<UserDto> response = callSync.execute();

        if (response.isSuccessful()) {
            String authorizationHeader = response.headers().get("Authorization");
            UserDto userDto = response.body();
            if (authorizationHeader != null) {
                String token = authorizationHeader.substring("Bearer ".length()).trim();
                log.info("Επιτυχής είσοδος του χρήστη: {} ", loginDto.getUsername());

                userDto.setAuthenticationHeader(token);
            }
            return userDto;
        }
        throw new IOException("Credentials were invalid");
    }
}