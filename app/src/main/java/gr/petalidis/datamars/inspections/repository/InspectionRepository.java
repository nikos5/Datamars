/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.slf4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.dto.InspectionDateProducer;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;

public class InspectionRepository {
    private static final Logger log = Log4jHelper.getLogger(InspectionRepository.class.getName());

    private InspectionRepository() {
    }

    public static boolean updateLocation(DbHandler dbHandler, UUID inspectionId, double latitude, double longitude) throws PersistenceException {
        try (SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            ContentValues values = new ContentValues();
            String[] selectionArgs = {inspectionId.toString()};
            values.put("locationX", latitude);
            values.put("locationY", longitude);

            int update = db.update(DbHandler.TABLE_INSPECTIONS, values, "id= ?", selectionArgs);

            if (update != 1) {
                log.error("Unable to update entry with id {}", inspectionId);
                throw new PersistenceException("Unable to update entry with id " + inspectionId.toString());
            }
        }
        return true;
    }

    public static Inspection save(DbHandler dbHandler, Inspection inspection) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        try (SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            ContentValues values = new ContentValues();
            values.put("id", inspection.getId().toString());
            values.put("producer1Name", inspection.getProducer1Name());
            values.put("producer1Tin", inspection.getProducer1Tin());
            values.put("producer2Name", inspection.getProducer2Name());
            values.put("producer2Tin", inspection.getProducer2Tin());
            values.put("producer3Name", inspection.getProducer3Name());
            values.put("producer3Tin", inspection.getProducer3Tin());
            values.put("producer4Name", inspection.getProducer4Name());
            values.put("producer4Tin", inspection.getProducer4Tin());
            values.put("locationX", inspection.getLatitude());
            values.put("locationY", inspection.getLongitude());
            values.put("date", dateFormat.format(inspection.getDate()));
            values.put("finalised", inspection.isFinalised() ? 1 : 0);
            values.put("uploaded", inspection.isUploaded() ? 1 : 0);
            values.put("totalInRegistryForProducer1", inspection.getTotalInRegistryForProducer1());
            values.put("totalInRegistryForProducer2", inspection.getTotalInRegistryForProducer2());
            values.put("totalInRegistryForProducer3", inspection.getTotalInRegistryForProducer3());
            values.put("totalInRegistryForProducer4", inspection.getTotalInRegistryForProducer4());

            db.insert(DbHandler.TABLE_INSPECTIONS, null, values);
        }
        return inspection;
    }

    public static List<InspectionDateProducer> getAllInspections(DbHandler dbHandler) {
        final List<InspectionDateProducer> inspectionDateProducerArrayList = new ArrayList<>();
        final String[] columns = {"id", "date", "producer1Name", "finalised", "uploaded"};
        final String selection = "";
        final String[] selectionArgs = {};
        final String orderBy = "date DESC";

        try (SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTIONS, columns, selection, selectionArgs, null, null, orderBy)) {

            if (cursor.moveToFirst()) {
                do {
                    InspectionDateProducer inspectionDateProducer = new InspectionDateProducer(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getInt(3) == 1,
                            cursor.getInt(4) == 1);
                    inspectionDateProducerArrayList.add(inspectionDateProducer);
                } while (cursor.moveToNext());
            }
        }
        return inspectionDateProducerArrayList;
    }

    public static boolean hasInspectionWithId(DbHandler dbHandler, String uuidString) {
        final String[] columns = {"id"};
        final String selection = "id = ?";
        final String[] selectionArgs = {uuidString};

        try (SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTIONS, columns, selection, selectionArgs, null, null, null)) {
            return cursor.getCount() != 0;
        }
    }

    public static Inspection getInspectionFor(DbHandler dbHandler, String uuidString) throws ParseException, PersistenceException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        final String[] columns = {"id", "date",
                "producer1Tin", "producer1Name",
                "producer2Tin", "producer2Name",
                "producer3Tin", "producer3Name",
                "producer4Tin", "producer4Name",
                "locationX", "locationY",
                "finalised", "uploaded",
                "totalInRegistryForProducer1", "totalInRegistryForProducer2",
                "totalInRegistryForProducer3", "totalInRegistryForProducer4"};
        final String selection = "id = ?";
        final String[] selectionArgs = {uuidString};

        UUID uuid = UUID.fromString(uuidString);
        try (SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTIONS, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.getCount() != 1) {
                throw new PersistenceException("Expected one entry, found none or too many");
            }
            cursor.moveToFirst();

            Inspection inspection = new Inspection(
                    dateFormat.parse(cursor.getString(1)),
                    cursor.getString(2),
                    cursor.getString(3));
            inspection.setId(uuid);
            inspection.setProducer2Tin(cursor.getString(4));
            inspection.setProducer2Name(cursor.getString(5));
            inspection.setProducer3Tin(cursor.getString(6));
            inspection.setProducer3Name(cursor.getString(7));
            inspection.setProducer4Tin(cursor.getString(8));
            inspection.setProducer4Name(cursor.getString(9));
            inspection.setLatitude(cursor.getFloat(10));
            inspection.setLongitude(cursor.getFloat(11));
            inspection.setEntries(EntryRepository.getEntriesFor(dbHandler, inspection.getId()));
            inspection.setScannedDocuments(ScannedDocumentRepository.getScannedDocumentFor(dbHandler, inspection.getId()));
            inspection.setConventionalTags(OtherEntryRepository.getEntriesFor(dbHandler, inspection.getId()));
            inspection.setFinalised(cursor.getInt(12) == 1);
            inspection.setUploaded(cursor.getInt(13) == 1);
            inspection.setTotalInRegistryForProducer1(cursor.getInt(14));
            inspection.setTotalInRegistryForProducer2(cursor.getInt(15));
            inspection.setTotalInRegistryForProducer3(cursor.getInt(16));
            inspection.setTotalInRegistryForProducer4(cursor.getInt(17));

            return inspection;
        }
    }

    public static boolean updateUploadedTrue(DbHandler dbHandler, UUID id) throws PersistenceException {
        try (SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            ContentValues values = new ContentValues();
            String[] selectionArgs = {id.toString()};
            values.put("uploaded", 1);

            int update = db.update(DbHandler.TABLE_INSPECTIONS, values, "id= ?", selectionArgs);

            if (update != 1) {
                log.error("Unable to update entry with id {}", id);
                throw new PersistenceException("Unable to update entry with id " + id.toString());
            }
        }
        return true;
    }

    public static Inspection update(DbHandler dbHandler, Inspection inspection) throws PersistenceException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        try (SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            ContentValues values = new ContentValues();
            String[] selectionArgs = {inspection.getId().toString()};
            values.put("producer1Name", inspection.getProducer1Name());
            values.put("producer1Tin", inspection.getProducer1Tin());
            values.put("producer2Name", inspection.getProducer2Name());
            values.put("producer2Tin", inspection.getProducer2Tin());
            values.put("producer3Name", inspection.getProducer3Name());
            values.put("producer3Tin", inspection.getProducer3Tin());
            values.put("producer4Name", inspection.getProducer4Name());
            values.put("producer4Tin", inspection.getProducer4Tin());
            values.put("locationX", inspection.getLatitude());
            values.put("locationY", inspection.getLongitude());
            values.put("date", dateFormat.format(inspection.getDate()));
            values.put("finalised", inspection.isFinalised() ? 1 : 0);
            values.put("uploaded", inspection.isUploaded() ? 1 : 0);
            values.put("totalInRegistryForProducer1", inspection.getTotalInRegistryForProducer1());
            values.put("totalInRegistryForProducer2", inspection.getTotalInRegistryForProducer2());
            values.put("totalInRegistryForProducer3", inspection.getTotalInRegistryForProducer3());
            values.put("totalInRegistryForProducer4", inspection.getTotalInRegistryForProducer4());

            int update = db.update(DbHandler.TABLE_INSPECTIONS, values, "id= ?", selectionArgs);

            if (update != 1) {
                log.error("Unable to update entry with id {}", inspection.getId());
                throw new PersistenceException("Unable to update entry with id " + inspection.getId().toString());
            }
        }
        return inspection;
    }

    public static void delete(DbHandler dbHandler, UUID inspectionId) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            String[] selectionArgs = {inspectionId.toString()};
            int deleted = db.delete(DbHandler.TABLE_INSPECTIONS, "id = ?", selectionArgs);
            log.info("Deleted {} number of entries with inspection id {}", deleted, inspectionId);
        }
    }
}
