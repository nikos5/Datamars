/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.dto.InspectionDateProducer;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;
import gr.petalidis.datamars.inspections.ui.login.LoginActivity;
import gr.petalidis.datamars.rsglibrary.RsgSessionFiles;

public class CreateInspectionActivity extends AppCompatActivity {

    private static final String DATES = "dates";
    private static final Logger log = Log4jHelper.getLogger(CreateInspectionActivity.class.getName());
    private RsgSessionFiles files = new RsgSessionFiles();
    private CreateInspectionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            files = (RsgSessionFiles) savedInstanceState.getSerializable(DATES);
            if (files == null) {
                files = new RsgSessionFiles();
            }
        }

        setContentView(R.layout.activity_calendar);

        Intent intent = getIntent();
        if (intent != null) {
            files = (RsgSessionFiles) intent.getSerializableExtra(DATES);
            if (files == null) {
                files = new RsgSessionFiles();
            }
        }

        setContentView(R.layout.activity_create_inspection);

        restore();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        menu.removeItem(R.id.temp_save);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.about) {
            PackageManager packageManager = this.getPackageManager();
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                        //Do Nothing
                    });
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                        "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                        "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                builder.show();
            } catch (PackageManager.NameNotFoundException e) {
                log.error("Could not show about message {}", e.getLocalizedMessage());
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putSerializable(DATES, files);
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        files = (RsgSessionFiles) savedInstanceState.getSerializable(DATES);
        if (files == null) {
            files = new RsgSessionFiles();
        }
        restore();
    }

    @Override
    protected void onResume() {
        super.onResume();
        restore();
    }

    private void restore() {
        DbHandler dbHandler = new DbHandler(this.getApplicationContext());

        try {
            List<InspectionDateProducer> inspectionDateProducerList = InspectionService.findAllInspections(dbHandler);

            final ListView listview = findViewById(R.id.inspectionList);

            adapter = new CreateInspectionAdapter(this,
                    android.R.layout.simple_list_item_1, inspectionDateProducerList);

            listview.setAdapter(adapter);


        } catch (PersistenceException e) {
            log.error(e.getLocalizedMessage());
            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void goToInspectionStepOneActivity(View view) {
        Intent intent = new Intent(this, InspectionStepOneActivity.class);
        intent.putExtra("files", files);
        startActivity(intent);
    }

    public void selectInspectionsToSend(View view) {
        final ListView listview = findViewById(R.id.inspectionList);
        adapter.selectInspectionsToSend(listview);
        findViewById(R.id.resetSelectedInspections).setVisibility(View.VISIBLE);
        findViewById(R.id.emptySpace).setVisibility(View.VISIBLE);
        findViewById(R.id.createInspectionButton).setVisibility(View.GONE);
        ImageButton selectInspectionsToSend = findViewById(R.id.selectInspectionsToSend);
        selectInspectionsToSend.setImageResource(R.drawable.ic_baseline_cloud_done_burgundy_64);
        selectInspectionsToSend.setOnClickListener(x ->
        {
            List<InspectionDateProducer> selectedInspections = adapter.getSelectedInspections(listview);
            resetSelectedInspections(view);

            if (!selectedInspections.isEmpty()) {
                List<String> idList = selectedInspections.stream().map(InspectionDateProducer::getId).collect(Collectors.toList());
                Intent intent = new Intent(this, LoginActivity.class);
                intent.putStringArrayListExtra("inspections", new ArrayList<>(idList));
                startActivity(intent);
            }
        });
    }

    public void resetSelectedInspections(View view) {
        final ListView listview = findViewById(R.id.inspectionList);
        adapter.resetSelectedInspections(listview);
        ImageButton selectInspectionsToSend = findViewById(R.id.selectInspectionsToSend);
        selectInspectionsToSend.setOnClickListener(this::selectInspectionsToSend);
        selectInspectionsToSend.setImageResource(R.drawable.ic_baseline_cloud_upload_burgundy_64);
        findViewById(R.id.resetSelectedInspections).setVisibility(View.GONE);
        findViewById(R.id.emptySpace).setVisibility(View.GONE);
        findViewById(R.id.createInspectionButton).setVisibility(View.VISIBLE);


    }
}
