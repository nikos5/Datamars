/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.apache.poi.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;

import gr.petalidis.datamars.inspections.domain.ScannedDocument;
import gr.petalidis.datamars.inspections.dto.util.UUIDGenerator;


public class DocumentRecordDto {
    private String filename = "";
    private String documentTypeName = "";
    private String mimetype = "text/plain";
    @JsonIgnore
    private byte[] data = new byte[0];
    private UUID documentId = UUIDGenerator.generate();

    public DocumentRecordDto() {
    }

    public DocumentRecordDto(ScannedDocument scannedDocument) {

        File source = new File(scannedDocument.getImagePath());
        try (InputStream in = new FileInputStream(source)) {
            byte[] bytes = IOUtils.toByteArray(in);
            data = bytes;
            filename = source.getName();
            mimetype = "image/jpeg";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getDocumentTypeName() {
        return documentTypeName;
    }

    public void setDocumentTypeName(String documentTypeName) {
        this.documentTypeName = documentTypeName;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String fileName) {
        this.filename = fileName;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    @JsonIgnore
    public byte[] getData() {
        return data;
    }

    @JsonIgnore
    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocumentRecordDto)) return false;
        DocumentRecordDto that = (DocumentRecordDto) o;
        return documentId.equals(that.documentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentId);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return "".equals(filename);
    }

}
