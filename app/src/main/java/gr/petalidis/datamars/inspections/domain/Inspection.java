/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import gr.petalidis.datamars.inspections.dto.ThumbnailDto;
import gr.petalidis.datamars.rsglibrary.Rsg;

public class Inspection implements Serializable {
    private static final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    private UUID id = UUID.randomUUID();

    private Date date;

    private String producer1Tin;
    private String producer1Name;
    private String producer2Tin = "";
    private String producer2Name = "";
    private String producer3Tin = "";
    private String producer3Name = "";
    private String producer4Tin = "";
    private String producer4Name = "";

    private int totalInRegistryForProducer1 = 0;
    private int totalInRegistryForProducer2 = 0;
    private int totalInRegistryForProducer3 = 0;
    private int totalInRegistryForProducer4 = 0;

    private List<Entry> entries = new ArrayList<>();

    private Set<OtherEntry> conventionalTags = new HashSet<>();

    private List<ScannedDocument> scannedDocuments = new ArrayList<>();

    private double latitude = 0.0f;
    private double longitude = 0.0f;

    private boolean finalised = false;

    private boolean uploaded = false;

    public Inspection(Date date, String producer1Tin, String producer1Name) {
        this.date = date;
        this.producer1Tin = producer1Tin;
        this.producer1Name = producer1Name;
    }

    public Inspection generateInspectionFor(Inspectee producer) {
        Inspection otherInspection = new Inspection(getDate(), producer.getTin(), producer.getName());

        otherInspection.setTotalInRegistryForProducer1(getTotalInRegistryForProducer(producer.getTin()));
        Set<OtherEntry> conventionalTagsFor = getConventionalTagsFor(producer.getTin());
        otherInspection.setConventionalTags(conventionalTagsFor);
        otherInspection.getConventionalTags().forEach(x -> x.setInspectionId(otherInspection.getId()));
        otherInspection.setLatitude(getLatitude());
        otherInspection.setLongitude(getLongitude());
        getEntries().stream()
                .filter(x -> x.getProducerTin().equals(producer.getTin())).forEach(x -> x.setInspectionId(otherInspection.getId()));
        otherInspection.setEntries(getEntries().stream()
                .filter(x -> x.getInspectionId().equals(otherInspection.getId())).collect(Collectors.toList()));
        getEntries().removeAll(otherInspection.getEntries());
        getConventionalTags().removeAll(otherInspection.getConventionalTags());
        otherInspection.setFinalised(isFinalised());
        return otherInspection;
    }

    public void clearOtherInspectees() {
        setConventionalTags(getConventionalTags().stream().filter(x -> x.getInspectee().getTin().equals(getProducer1Tin())).collect(Collectors.toSet()));
        setEntries(getEntries().stream().filter(x -> x.getInspectionId().equals(getId())).collect(Collectors.toList()));

        producer4Name = producer3Name = producer2Name = "";
        producer4Tin = producer3Tin = producer2Tin = "";
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProducer1Tin() {
        return producer1Tin;
    }

    public String getProducer1Name() {
        return producer1Name;
    }

    public String getProducer2Tin() {
        return producer2Tin;
    }

    public boolean hasProducer2() {
        return !producer2Tin.isEmpty() && !producer2Tin.equals(Inspectee.getDummyInspectee().getTin());
    }

    public void setProducer2Tin(String producer2Tin) {
        this.producer2Tin = producer2Tin;
    }

    public String getProducer2Name() {
        return producer2Name;
    }

    public void setProducer2Name(String producer2Name) {
        this.producer2Name = producer2Name;
    }

    public String getProducer3Tin() {
        return producer3Tin;
    }

    public boolean hasProducer3() {
        return !producer3Tin.isEmpty() && !producer3Tin.equals(Inspectee.getDummyInspectee().getTin());
    }

    public void setProducer3Tin(String producer3Tin) {
        this.producer3Tin = producer3Tin;
    }

    public String getProducer3Name() {
        return producer3Name;
    }

    public void setProducer3Name(String producer3Name) {
        this.producer3Name = producer3Name;
    }

    public String getProducer4Tin() {
        return producer4Tin;
    }

    public boolean hasProducer4() {
        return !producer4Tin.isEmpty() && !producer4Tin.equals(Inspectee.getDummyInspectee().getTin());
    }

    public void setProducer4Tin(String producer4Tin) {
        this.producer4Tin = producer4Tin;
    }

    public String getProducer4Name() {
        return producer4Name;
    }

    public void setProducer4Name(String producer4Name) {
        this.producer4Name = producer4Name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public boolean hasUnassignedEntries() {
        return entries.stream().allMatch(x -> x.getProducerTin().isEmpty());
    }

    public List<Entry> getEntriesFor(String tin) {
        return entries.stream().filter(x -> x.getProducerTin().equals(tin)).collect(Collectors.toList());
    }

    public String getMostFrequentOwnerTagFor(String tin) {
        return getEntriesFor(tin).stream().map(Entry::getOwner).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream().max((o1, o2) -> o1.getValue().compareTo(o2.getValue()))
                .map(Map.Entry::getKey).orElse(getOwnerTags().stream().findAny().orElse(""));
    }

    public boolean hasOwnerTag(String tag, String tin) {
        return getEntriesFor(tin)
                .stream()
                .anyMatch(x -> x.getOwner().equals(tag));
    }

    public List<ScannedDocument> getScannedDocuments() {
        return scannedDocuments;
    }

    public void setScannedDocuments(List<ScannedDocument> scannedDocuments) {
        this.scannedDocuments = scannedDocuments;
    }

    public void initScannedDocuments(List<ThumbnailDto> thumbnailDtos) {
        this.scannedDocuments = thumbnailDtos.stream().map(x -> new ScannedDocument(this.id, x.getImagePath())).collect(Collectors.toList());
    }

    public ScannedDocument addThumbnail(ThumbnailDto thumbnailDto) {
        ScannedDocument scannedDocument = new ScannedDocument(this.id, thumbnailDto.getImagePath());
        this.scannedDocuments.add(scannedDocument);
        return scannedDocument;
    }

    public void initEntries(Set<Rsg> rsgs) {
        Map<Rsg, Set<Rsg>> doubles = Rsg.getEntriesMatching(rsgs, rsg -> rsg::isDoubleOf);
        Map<Rsg, Set<Rsg>> errors = Rsg.getEntriesMatching(rsgs, rsg -> rsg::isInErrorOf);

        Set<Rsg> flattenedDoubles = doubles.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
        Set<Rsg> flattenedErrors = errors.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());

        this.entries = rsgs
                .stream()
                .map(x -> {
                    Entry entry = new Entry(this,
                            x.getCountryCode(),
                            x.getIdentificationCode(),
                            x.getDate());
                    if (flattenedDoubles.contains(x)) {
                        entry.setComment(CommentType.DOUBLE);
                        entry.setInRegister(false);
                    }
                    if (flattenedErrors.contains(x)) {
                        entry.setComment(CommentType.FAULT);
                        entry.setInRegister(false);
                    }
                    return entry;
                })
                .collect(Collectors.toList());
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public Set<String> getOwnerTags() {
        return entries.stream().map(x -> x.getTag().substring(4, 8)).collect(Collectors.toSet());
    }

    public List<Entry> getValidEntries() {
        return this.entries.stream()
                .filter(x -> !x.getProducer().isEmpty()
                        && !x.getProducer().equals("Άλλος")).collect(Collectors.toList());
    }

    public Set<OtherEntry> getConventionalTags() {
        return conventionalTags;
    }

    public Set<OtherEntry> getConventionalTagsFor(String tin) {
        return conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(tin)).collect(Collectors.toSet());
    }

    public void setLegalConventionalTag(Inspectee inspectee, String animal, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, animal, number, OtherEntryType.CONVENTIONAL, id));
        conventionalTags.add(new OtherEntry(inspectee, animal, number, OtherEntryType.CONVENTIONAL, id));
    }

    public void setConventionalTags(Set<OtherEntry> conventionalTags) {
        this.conventionalTags = conventionalTags;
    }

    public List<Inspectee> getProducers() {
        List<Inspectee> names = getProducersWithNoDummy();
        names.add(Inspectee.getDummyInspectee());
        return names;
    }

    public boolean isFinalised() {
        return finalised;
    }

    public void setFinalised(boolean finalised) {
        this.finalised = finalised;
    }

    public List<Inspectee> getProducersWithNoDummy() {
        List<Inspectee> names = new ArrayList<>();
        names.add(new Inspectee(producer1Tin, producer1Name));
        if (!producer2Name.trim().isEmpty()) names.add(new Inspectee(producer2Tin, producer2Name));
        if (!producer3Name.trim().isEmpty()) names.add(new Inspectee(producer3Tin, producer3Name));
        if (!producer4Name.trim().isEmpty()) names.add(new Inspectee(producer4Tin, producer4Name));
        return names;
    }

    //Καταμετρηθέντα + Οσα σκαναρίστηκαν εκτός των FALSE και αλόγων + όσα με συμβατικά ενώτια
    private long getCount(String producer1Tin) {
        long allCountElectronicTag = getEntries().stream().filter(x -> x.isNotDummy()
                && x.getProducerTin().equals(producer1Tin) && !x.getComment().equals(CommentType.FAULT)).count();

        long allCountElectronicTagWithoutHorses = allCountElectronicTag -
                getCount(producer1Tin, AnimalType.HORSE_ANIMAL);
        long conventionalTagCount =
                conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producer1Tin))
                        .map(OtherEntry::getCount).reduce(0, Integer::sum);
        return allCountElectronicTagWithoutHorses + conventionalTagCount;
    }

    //Καταμετρηθέντα
    private long getCount(String producer1Tin, AnimalType animalType) {
        return getEntries().stream().filter(x -> x.isNotDummy()
                && x.getAnimalType().trim().equals(animalType.title) && x.getProducerTin()
                .equals(producer1Tin)).count()
                + conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producer1Tin)
                && x.getAnimal().equals(animalType.title)).map(OtherEntry::getCount).reduce(0, Integer::sum);
    }

    public float getSinglesPct(String producerTin, String animals) {
        long singleElectronic = getEntries().stream().filter(x -> x.isNotDummy()
                && x.isInRegister()
                && x.getComment().equals(CommentType.SINGLE)
                && x.getAnimalType().trim().equals(animals) && x.getProducerTin()
                .equals(producer1Tin)).count();
        long singleUnderOrder = getEntries().stream().filter(x -> x.isNotDummy()
                && x.isInRegister()
                && x.getComment().equals(CommentType.IN_ORDER)
                && x.getAnimalType().trim().equals(animals) && x.getProducerTin()
                .equals(producer1Tin)).count();

        Integer singleConventional = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producer1Tin)
                && x.getAnimal().equals(animals) &&
                x.getEntryType() == OtherEntryType.SINGLE).map(OtherEntry::getCount).reduce(0, Integer::sum);

        long allOthers = getEntries().stream().filter(x -> x.isNotDummy()
                && x.isInRegister()
                && !x.getComment().equals(CommentType.SLAUGHTERED)
                && !x.getComment().equals(CommentType.SOLD)
                && !x.getComment().equals(CommentType.DEAD)
                && !x.getComment().equals(CommentType.DOUBLE)
                && !x.getComment().equals(CommentType.FAULT)
                && !x.getComment().equals(CommentType.SINGLE)
                && !x.getComment().equals(CommentType.IN_ORDER)
                && x.getAnimalType().trim().equals(animals) && x.getProducerTin()
                .equals(producer1Tin)).count()
                +
                conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producer1Tin)
                        && x.getAnimal().equals(animals) && x.getEntryType() == OtherEntryType.CONVENTIONAL)
                        .map(OtherEntry::getCount).reduce(0, Integer::sum);

        long total = singleConventional + singleElectronic + singleUnderOrder + allOthers;
        long singles = singleConventional + singleElectronic + singleUnderOrder;
        if (total == 0) {
            return 0;
        }

        return ((float) singles) / total;
    }

    public boolean hasOver20PctSingles(String producerTin, String animals) {

        float pct = getSinglesPct(producerTin, animals);
        //Count animals with single tags only when they are <=20% of the total selectables
        return !(pct <= 0.2f);
    }

    //Επιλέξιμα: Όσα είναι στο μητρώο + όσα είναι νομιμα συμβατικα  (ανά είδος)
    private long getSelectableCount(String producer1Tin, String animals) {
        long singleUnderOrder = getEntries().stream().filter(x -> x.isNotDummy()
                && x.isInRegister()
                && x.getComment().equals(CommentType.IN_ORDER)
                && x.getAnimalType().trim().equals(animals) && x.getProducerTin()
                .equals(producer1Tin)).count();

        long allOthers = getEntries().stream().filter(x -> x.isNotDummy()
                && x.isInRegister()
                && !x.getComment().equals(CommentType.SLAUGHTERED)
                && !x.getComment().equals(CommentType.SOLD)
                && !x.getComment().equals(CommentType.DEAD)
                && !x.getComment().equals(CommentType.DOUBLE)
                && !x.getComment().equals(CommentType.FAULT)
                && !x.getComment().equals(CommentType.SINGLE)
                && !x.getComment().equals(CommentType.IN_ORDER)
                && x.getAnimalType().trim().equals(animals) && x.getProducerTin()
                .equals(producer1Tin)).count()
                +
                conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producer1Tin)
                        && x.getAnimal().equals(animals) && x.getEntryType() == OtherEntryType.CONVENTIONAL)
                        .map(OtherEntry::getCount).reduce(0, Integer::sum);

        long total = singleUnderOrder + allOthers;
        return total;
    }

    //Ζώα χωρίς ενώτια ούτε ηλ. σήμανση:Εκτός ιστορικής περιόδου συμβατικά + νούμερο που εισάγει ο χρήστης + όσα έχουν χαρακτηριστεί διπλά
    private long getNoTag(String producerTin) {
        long doubles = getEntries().stream().filter(x -> x.isNotDummy()
                && x.getComment().equals(CommentType.DOUBLE)
                && x.getProducerTin()
                .equals(producerTin)).count();

        Integer untagged = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.hasNoEarringAndIsOver6()).map(OtherEntry::getCount).reduce(0, Integer::sum);
        return doubles + untagged;
    }

    //Χωρίς νόμιμη σήμανση (βασική)
    // Ζώα > 6 μηνών χωρίς σήμανση
    // + Ζώα με διπλά ενώτια (CommentType.DOUBLE)
    // + Ζώα με μη αποδεκτά ενώτια (CommentType.Invalid)
    // + Ζώα με μονό ενώτιο χωρίς παραγγελία (είτε CommentType.SINGLE είτε OtherEntryType.SINGLE)
    // + Ζώα με συμβατικά μετά το 2010

    private long getNoLegalTag(String producerTin) {
        long doubles = getEntries().stream().filter(x -> x.isNotDummy()
                && x.getComment().equals(CommentType.DOUBLE)
                && x.getProducerTin()
                .equals(producerTin)).count();
        long invalids = getEntries().stream().filter(x -> x.isNotDummy()
                && x.getComment().equals(CommentType.INVALID)
                && x.getProducerTin()
                .equals(producerTin)).count();
        long electronicSingles = getEntries().stream().filter(x -> x.isNotDummy()
                && x.getComment().equals(CommentType.SINGLE)
                && x.getProducerTin()
                .equals(producerTin)).count();

        long conventionalSingles = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.getEntryType() == OtherEntryType.SINGLE).map(OtherEntry::getCount).reduce(0, Integer::sum);

        Integer conventionalsAfter2010 = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.getEntryType() == OtherEntryType.ILLEGAL).map(OtherEntry::getCount).reduce(0, Integer::sum);

        Integer untaggedOver6Months = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.hasNoEarringAndIsOver6()).map(OtherEntry::getCount).reduce(0, Integer::sum);
        return untaggedOver6Months + doubles + invalids + electronicSingles + conventionalSingles + conventionalsAfter2010;
    }

    //Πολλαπλή:
    //Χωρίς ηλ. σήμανση: συμβατικά παράνομα
    private long getNoElectronicTag(String producerTin) {
        return conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && (x.getEntryType() == OtherEntryType.ILLEGAL)).map(OtherEntry::getCount).reduce(0, Integer::sum);
    }

    // Πολλαπλή:
    // > 6 μηνών χωρίς ενώτια ούτε ηλεκτρονική σήμανση
    // Διπλά, Άκυρα, > 6 μηνών χωρίς σήμανση
    private long getOver6NoTagsAndNoElectronicTags(String producerTin) {
        long doublesAndInvalids = entries.stream().filter(x -> x.getComment() == CommentType.DOUBLE || x.getComment() == CommentType.INVALID).count();

        return doublesAndInvalids + conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && (
                (x.hasNoEarringAndIsOver6()))).map(OtherEntry::getCount).reduce(0, Integer::sum);
    }

    //Ζώα με ένα ένωτιο: Είτε συμβατικό είτε ηλεκτρονικό
    private long getUniqueTag(String producerTin) {
        return getEntries().stream()
                .filter(x -> x.isNotDummy()
                        && x.isInRegister()
                        && x.getProducerTin().equals(producerTin)
                        && x.getComment().equals(CommentType.SINGLE)).count()
                + conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.getEntryType() == OtherEntryType.SINGLE).map(OtherEntry::getCount).reduce(0, Integer::sum);
    }

    //Ζώα με ένα ένωτιο αλλά με παραγγελία
    private long getUniqueTagInOrder(String producerTin) {
        return getEntries().stream()
                .filter(x -> x.isNotDummy()
                        && x.isInRegister()
                        && x.getProducerTin().equals(producerTin)
                        && x.getComment().equals(CommentType.IN_ORDER)).count();
    }

    //Καταμετρηθέντα ζώα με σήμανση που δεν αναγράφονται στο μητρώο: Όσα δεν έχουν τσεκ στο μητρώο (απο ηλεκτρονικά)
    //+ νόμιμα συμβατικά που δεν είναι στο μητρώο
    // + χωρίς ενώτιο κάτω των 6 μηνών εκτός μητρώου
    private long getOutOfRegistryTagged(String producerTin) {
        long outOfRegistry = getEntries().stream().filter(x -> x.isNotDummy()
                && !x.isInRegister()
                && !x.getComment().equals(CommentType.DOUBLE)
                && !x.getComment().equals(CommentType.INVALID)
                && !x.getComment().equals(CommentType.FAULT)
                && x.getProducerTin()
                .equals(producerTin)).count();

        Integer outOfRegistryUntagged = conventionalTags.stream().filter(x -> x.getInspectee().getTin().equals(producerTin)
                && x.getEntryType() == OtherEntryType.OUT_OF_REGISTRY //Χωρίς ενώτιο και άνω των 6 μηνών
        ).map(OtherEntry::getCount).reduce(0, Integer::sum);

        return outOfRegistry + outOfRegistryUntagged + getNoTagUnder6MonthOldOutOfRegistry(getInspecteeFor(producerTin));
    }

    public Inspectee getInspecteeFor(String tin) {
        if (tin.equals(Inspectee.getDummyInspectee().getTin())) {
            return Inspectee.getDummyInspectee();
        }
        if (tin.equals(producer1Tin)) {
            return new Inspectee(producer1Tin, producer1Name);
        } else if (tin.equals(producer2Tin)) {
            return new Inspectee(producer2Tin, producer2Name);
        } else if (tin.equals(producer3Tin)) {
            return new Inspectee(producer3Tin, producer3Name);
        }
        return new Inspectee(producer4Tin, producer4Name);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<String> toStrings() {
        List<String> strings = new ArrayList<>();
        String line0 = "Ημερομηνία:" + "," + format.format(date) + "\n";
        String line1 = "ΑΦΜ Παραγωγού:" + "," + producer1Tin + "," + "Ονοματεπώνυμο:" + "," + producer1Name;
        String line2 = "ΑΦΜ 1ου συστεγαζόμενου:" + "," + producer2Tin + "," + "Ονοματεπώνυμο:" + "," + producer2Name;
        String line3 = "ΑΦΜ 2ου συστεγαζόμενου:" + "," + producer3Tin + "," + "Ονοματεπώνυμο:" + "," + producer3Name;
        String line4 = "ΑΦΜ 3ου συστεγαζόμενου:" + "," + producer4Tin + "," + "Ονοματεπώνυμο:" + "," + producer4Name;

        String line5 = "Συντεταγμένες:" + "," + latitude + "," + longitude;
        strings.add(line0);
        strings.add(line1);
        strings.add(line2);
        strings.add(line3);
        strings.add(line4);
        strings.add(line4);
        strings.add(line5);
        String header = "Χώρα, Ενώτιο,Ημ/νια Ελέγχου,Βρέθηκε στο μητρώο,Ονοματεπώνυμο Παραγωγού,ΑΦΜ Παραγωγού,Είδος ζώου,Φυλή, Σχόλια";
        strings.add(header);
        strings.addAll(entries.stream().filter(Entry::isNotDummy).map(Entry::toString).collect(Collectors.toList()));
        return strings;
    }

    public void setSingleConventionalTag(Inspectee inspectee, String animal, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, animal, number, OtherEntryType.SINGLE, id));
        conventionalTags.add(new OtherEntry(inspectee, animal, number, OtherEntryType.SINGLE, id));
    }

    public void setIllegalConventionalTag(Inspectee inspectee, String animal, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, animal, number, OtherEntryType.ILLEGAL, id));
        conventionalTags.add(new OtherEntry(inspectee, animal, number, OtherEntryType.ILLEGAL, id));
    }

    public void setConventionalOutOfRegistry(Inspectee inspectee, String animal, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, animal, number, OtherEntryType.OUT_OF_REGISTRY, id));
        conventionalTags.add(new OtherEntry(inspectee, animal, number, OtherEntryType.OUT_OF_REGISTRY, id));
    }

    public int getLegalConventionalTagFor(Inspectee inspectee, String animal) {
        OtherEntry otherEntry = new OtherEntry(inspectee, animal, 0, OtherEntryType.CONVENTIONAL, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();
    }

    public int getSingleConventionalTagFor(Inspectee inspectee, String animal) {
        OtherEntry otherEntry = new OtherEntry(inspectee, animal, 0, OtherEntryType.SINGLE, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();
    }

    public int getIllegalConventionalTagFor(Inspectee inspectee, String animal) {
        OtherEntry otherEntry = new OtherEntry(inspectee, animal, 0, OtherEntryType.ILLEGAL, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();
    }

    public int getOutOfRegistryTagFor(Inspectee inspectee, String animal) {
        OtherEntry otherEntry = new OtherEntry(inspectee, animal, 0, OtherEntryType.OUT_OF_REGISTRY, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();

    }

    public void setNoTagUnder6MonthOutOfRegistry(Inspectee inspectee, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY.label, number, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY, id));
        conventionalTags.add(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY.label, number, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY, id));
    }

    public int getNoTagUnder6MonthOldOutOfRegistry(Inspectee inspectee) {
        OtherEntry otherEntry = new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY.label, 0, OtherEntryType.NO_EARRING_UNDER_6_OUT_OF_REGISTRY, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();
    }

    public void setNoTagUnder6Month(Inspectee inspectee, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6.label, number, OtherEntryType.NO_EARRING_UNDER_6, id));
        conventionalTags.add(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6.label, number, OtherEntryType.NO_EARRING_UNDER_6, id));
    }

    public void setNoTagOver6Month(Inspectee inspectee, int number) {
        conventionalTags.remove(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_OVER_6.label, number, OtherEntryType.NO_EARRING_OVER_6, id));
        conventionalTags.add(new OtherEntry(inspectee, OtherEntryType.NO_EARRING_OVER_6.label, number, OtherEntryType.NO_EARRING_OVER_6, id));
    }

    public int getNoTagOver6MonthOld(Inspectee inspectee) {
        OtherEntry otherEntry = new OtherEntry(inspectee, OtherEntryType.NO_EARRING_OVER_6.label, 0, OtherEntryType.NO_EARRING_OVER_6, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);
        return result.getCount();
    }

    public int getNoTagUnder6MonthOld(Inspectee inspectee) {
        OtherEntry otherEntry = new OtherEntry(inspectee, OtherEntryType.NO_EARRING_UNDER_6.label, 0, OtherEntryType.NO_EARRING_UNDER_6, id);
        OtherEntry result = conventionalTags.stream()
                .filter(x -> x.equals(otherEntry)).findAny().orElse(otherEntry);

        return result.getCount();
    }

    public Report generateReportFor(Inspectee inspectee) {
        Map<AnimalType, Long> selectablesMap = new HashMap<>();
        EnumSet.allOf(AnimalType.class).stream().filter(x -> x != AnimalType.KID_ANIMAL && x != AnimalType.LAMB_ANIMAL).forEach(animal ->
                selectablesMap.put(animal, getSelectableCount(inspectee.getTin(), animal.getTitle())));

        Map<AnimalType, Boolean> hasOver20PctMap = new HashMap<>();
        EnumSet.allOf(AnimalType.class).stream().filter(x -> x != AnimalType.KID_ANIMAL && x != AnimalType.LAMB_ANIMAL).forEach(animal ->
        {
            if (hasOver20PctSingles(inspectee.getTin(), animal.getTitle()))
                hasOver20PctMap.put(animal, true);
        });
        long kids = getSelectableCount(inspectee.getTin(), AnimalType.KID_ANIMAL.getTitle());
        long lambs = getSelectableCount(inspectee.getTin(), AnimalType.LAMB_ANIMAL.getTitle());

        selectablesMap.put(AnimalType.KIDLAMB_ANIMAL,
                kids +
                        lambs);

        return new Report(inspectee,
                getCount(inspectee.getTin()),
                getNoLegalTag(inspectee.getTin()),
                getNoTagUnder6MonthOld(inspectee),
                getNoElectronicTag(inspectee.getTin()),
                getUniqueTag(inspectee.getTin()),
                getOver6NoTagsAndNoElectronicTags(inspectee.getTin()),
                getOutOfRegistryTagged(inspectee.getTin()),
                getTotalInRegistryForProducer(inspectee.getTin()),
                selectablesMap,
                hasOver20PctMap
        );
    }

    public void setProducer1Tin(String producer1Tin) {
        this.producer1Tin = producer1Tin;
    }

    public void setProducer1Name(String producer1Name) {
        this.producer1Name = producer1Name;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public boolean hasValidProducers() {
        return !producer1Tin.equals("000000000");
    }

    public Entry createNewEntry() {
        Entry entry = new Entry();
        entry.setCountry("0030");
        entry.setTag("000000000000");
        entry.setComment(CommentType.SINGLE);
        entry.setInRegister(true);
        entry.setInspectionId(getId());
        entry.setTagDate(new Date());
        entry.setAutomatic(false);
        if (!getEntriesFor(getProducer1Tin()).isEmpty()) {
            Entry basicEntry = getEntriesFor(getProducer1Tin()).get(0);

            entry.setAnimalGenre(basicEntry.getAnimalGenre());
            entry.setAnimalType(basicEntry.getAnimalType());

            entry.setProducer(basicEntry.getProducer());
            entry.setProducerTin(basicEntry.getProducerTin());
            entry.setTag(basicEntry.getPrefectureTag() + basicEntry.getOwner() + "0000");
        }
        if (getEntries().contains(entry)) {
            entry.setComment(CommentType.DOUBLE);
        }
        return entry;
    }

    public int getTotalInRegistryForProducer(Inspectee inspectee) {
        return getTotalInRegistryForProducer(inspectee.getTin());
    }

    public int getTotalInRegistryForProducer(String tin) {
        if (tin.isEmpty() || tin.equals(Inspectee.getDummyInspectee().getTin())) {
            return 0;
        }
        if (tin.equals(producer1Tin)) {
            return getTotalInRegistryForProducer1();
        } else if (tin.equals(producer2Tin)) {
            return getTotalInRegistryForProducer2();
        } else if (tin.equals(producer3Tin)) {
            return getTotalInRegistryForProducer3();
        }
        return getTotalInRegistryForProducer4();
    }

    public void setTotalInRegistryForProducer(String tin, int totalInRegistryForProducer) {
        if (tin.isEmpty() || tin.equals(Inspectee.getDummyInspectee().getTin())) {
            return;
        }
        if (tin.equals(producer1Tin)) {
            setTotalInRegistryForProducer1(totalInRegistryForProducer);
        } else if (tin.equals(producer2Tin)) {
            setTotalInRegistryForProducer2(totalInRegistryForProducer);
        } else if (tin.equals(producer3Tin)) {
            setTotalInRegistryForProducer3(totalInRegistryForProducer);
        }
        setTotalInRegistryForProducer4(totalInRegistryForProducer);
    }

    public int getTotalInRegistryForProducer1() {
        return totalInRegistryForProducer1;
    }

    public void setTotalInRegistryForProducer1(int totalInRegistryForProducer1) {
        this.totalInRegistryForProducer1 = totalInRegistryForProducer1;
    }

    public int getTotalInRegistryForProducer2() {
        return totalInRegistryForProducer2;
    }

    public void setTotalInRegistryForProducer2(int totalInRegistryForProducer2) {
        this.totalInRegistryForProducer2 = totalInRegistryForProducer2;
    }

    public int getTotalInRegistryForProducer3() {
        return totalInRegistryForProducer3;
    }

    public void setTotalInRegistryForProducer3(int totalInRegistryForProducer3) {
        this.totalInRegistryForProducer3 = totalInRegistryForProducer3;
    }

    public int getTotalInRegistryForProducer4() {
        return totalInRegistryForProducer4;
    }

    public void setTotalInRegistryForProducer4(int totalInRegistryForProducer4) {
        this.totalInRegistryForProducer4 = totalInRegistryForProducer4;
    }

    public List<Entry> getInvalidEntries() {
        return this.entries.stream()
                .filter(x -> x.getProducer().isEmpty()
                        || x.getProducer().equals("Άλλος")).collect(Collectors.toList());
    }
}
