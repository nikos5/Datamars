
/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.inspections.domain.AnimalType;
import gr.petalidis.datamars.inspections.domain.Entry;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.domain.Report;

public class ExcelService {
    private static final Logger log = Log4jHelper.getLogger(ExcelService.class.getName());

    private ExcelService() {
    }

    public static String export(Inspection inspection, String filepath, String name) throws IOException {

        File file = new File(filepath, name);

        try (Workbook workbook = new HSSFWorkbook()) {

            int counter = 0;
            //fix name to contain only legal characters
            for (Inspectee inspectee : inspection.getProducersWithNoDummy()) {
                String canonisedName = inspectee.getName().replaceAll("[^A-Za-z0-9]", "_");
                Sheet sheet = workbook.createSheet(inspectee.getTin() + "_" + canonisedName + "_" + counter);
                counter++;
                Row headerRow = sheet.createRow(1);
                Cell cell = headerRow.createCell(0);
                cell.setCellValue("Παραγωγός");
                cell = headerRow.createCell(1);
                cell.setCellValue(inspectee.getName());
                cell = headerRow.createCell(2);
                cell.setCellValue("ΑΦΜ");
                cell.setCellValue(inspectee.getTin());

                cell = headerRow.createCell(3);
                cell.setCellValue("Συντεταγμένες:");
                cell = headerRow.createCell(4);
                cell.setCellValue(inspection.getLatitude() + "," + inspection.getLongitude());
                Row titleRow = sheet.createRow(2);
                cell = titleRow.createCell(0);
                cell.setCellValue("Χώρα");
                cell = titleRow.createCell(1);
                cell.setCellValue("Ενώτιο");
                cell = titleRow.createCell(2);
                cell.setCellValue("Ημερομηνία");
                cell = titleRow.createCell(3);
                cell.setCellValue("Στο μητρώο;");
                cell = titleRow.createCell(4);
                cell.setCellValue("Είδος");
                cell = titleRow.createCell(5);
                cell.setCellValue("Φυλή");
                cell = titleRow.createCell(6);
                cell.setCellValue("Σχόλια");

                int j = 3;
                for (Entry entry : inspection.getEntriesFor(inspectee.getTin())) {
                    Row entryRow = sheet.createRow(j++);
                    cell = entryRow.createCell(0);
                    cell.setCellValue(entry.getCountry());
                    cell = entryRow.createCell(1);
                    cell.setCellValue(entry.getTag());
                    cell = entryRow.createCell(2);
                    cell.setCellValue(Moo.getTimestampFormatter().format(entry.getTagDate()));
                    cell = entryRow.createCell(3);
                    cell.setCellValue(entry.isInRegister() ? "ΝΑΙ" : "ΟΧΙ");
                    cell = entryRow.createCell(4);
                    cell.setCellValue(entry.getAnimalType());
                    cell = entryRow.createCell(5);
                    cell.setCellValue(entry.getAnimalGenre());
                    cell = entryRow.createCell(6);
                    cell.setCellValue(entry.getComment().getTitle());
                }
                j++;
                Report report = inspection.generateReportFor(inspectee);
                Row entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Στοιχεία αναγραφόμενων στο μητρώο");
                cell = entryRow.createCell(1);
                cell.setCellValue(inspection.getTotalInRegistryForProducer(inspectee.getTin()));

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Καταμετρημένα ζώα");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getTotal());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Αναγραφόμενα στο μητρώο");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getTotalInRegistry());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Χωρίς νόμιμη σήμανση");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getNoLegalTag());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Με νόμιμη σήμανση που δεν αναγράφονται στο μητρώο:");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getCountedButNotInRegistry());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Πολλαπλή συμμόρφωση");
                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Μετά το 2010 χωρίς ηλ. σήμανση:");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getNoElectronicTag());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("> 6 μηνών με ένα ενώτιο:");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getSingleTag());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue(">6 μηνών χωρίς ενώτια ούτε ηλ. σήμανση:");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getOver6NoTagsAndNoElectronicTags());

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Σύνολο καταγεγραμμένων στο μητρώο:");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getTotalInRegistry());

                for (AnimalType animalType : EnumSet.allOf(AnimalType.class)) {
                    if (animalType != AnimalType.KID_ANIMAL && animalType != AnimalType.LAMB_ANIMAL) {
                        if (inspection.hasOver20PctSingles(inspectee.getTin(), animalType.getTitle())) {
                            entryRow = sheet.createRow(j++);
                            cell = entryRow.createCell(0);
                            cell.setCellValue("ΠΟΙΝΗ ΠΟΛΛΑΠΛΗΣ: Ποσοστό μονών ενωτίων για :" + animalType.getTitle());
                            cell = entryRow.createCell(1);
                            cell.setCellValue(inspection.getSinglesPct(inspectee.getTin(), animalType.getTitle()));
                        }
                    }
                }

                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Επιλέξιμα ζώα:");
                entryRow = sheet.createRow(j++);
                cell = entryRow.createCell(0);
                cell.setCellValue("Αμνοερίφια χωρίς σήμανση κάτω των 6 μηνών");
                cell = entryRow.createCell(1);
                cell.setCellValue(report.getNoTagUnder6());
                Map<AnimalType, Long> selectables = report.getSelectable();
                for (AnimalType animalType : selectables.keySet()) {
                    entryRow = sheet.createRow(j++);
                    cell = entryRow.createCell(0);
                    cell.setCellValue(animalType.getTitle());
                    cell = entryRow.createCell(1);
                    cell.setCellValue(selectables.get(animalType));
                }

                //          `
            }




            if (file.exists()) {
                boolean delete = file.delete();
                if (!delete) {
                    log.error("File with same name already present that I could not overwrite: {}", file.getAbsolutePath());
                    throw new IOException("File with same name already present that I could not overwrite");
                }
            }
            if (file.createNewFile()) {
                try (
                        FileOutputStream fOut = new FileOutputStream(file)
                ) {
                    workbook.write(fOut);
                }
                return file.getAbsolutePath();

            }
        }
        log.error("Could not write file {}", file.getAbsolutePath());
        throw new IOException("Could not write file " + file.getAbsolutePath());
    }
}
