/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.domain;


import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Report {
    private final Inspectee inspectee;

    private final long total; //Καταμετρηθέντα
    private final long noLegalTag; //Ζώα χωρίς νόμιμη σήμανση
    private final long noTagUnder6; //Ζώα χωρίς σήμανση < 6 μηνών
    private final long noElectronicTag; //Ζώα μετά το 2010 χωρίς ηλεκτρονική σήμανση
    private final long singleTag; //Ζώα > 6 μηνών με ένα μόνο νέο μέσο σήμανσης
    private final long over6NoTagsAndNoElectronicTags; //Ζώα > 6μηνών χωρίς ηλεκτρονική σήμανση και χωρίς ενώτιο
    private final long countedButNotInRegistry; //Καταμετρηθέντα ζώα με σήμανση που δεν αναγράφονται στο μητρώο

    private final Map<AnimalType, Long> selectable;
    private final Map<AnimalType, Boolean> hasOver20PctMap;
    private final long totalInRegistry;

    public Report(Inspectee inspectee, long total,
                  long noLegalTag, long noTagUnder6, long noElectronicTag, long singleTag,
                  long getOver6NoTagsAndNoElectronicTags,
                  long countedButNotInRegistry, long totalInRegistry, Map<AnimalType, Long> selectable,
                  Map<AnimalType, Boolean> hasOver20PctMap) {
        this.inspectee = inspectee;
        this.total = total;
        this.noLegalTag = noLegalTag;
        this.noTagUnder6 = noTagUnder6;
        this.noElectronicTag = noElectronicTag;
        this.singleTag = singleTag;
        this.over6NoTagsAndNoElectronicTags = getOver6NoTagsAndNoElectronicTags;
        this.countedButNotInRegistry = countedButNotInRegistry;
        this.selectable = selectable;
        this.hasOver20PctMap = hasOver20PctMap;
        this.totalInRegistry = totalInRegistry;
    }

    private String surround(String id, String value) {
        return "<span id=\""+inspectee.getTin()+"-"+id+"\">" + value + "</span>";

    }
    private String surround(String id, long value) {
        return surround(id,value+"");

    }

    public long getTotal() {
        return total;
    }

    private String getTotalHtmlString() {
        return surround("total", getTotal());
    }


    public long getNoLegalTag() {
        return noLegalTag;
    }

    private String getNoLegalTagHtmlString() {
        return surround("noLegalΤag", getNoLegalTag());
    }

    public long getNoTagUnder6() {
        return noTagUnder6;
    }

    private String getNoTagUnder6HtmlString() {
        return surround("noTagUnder6", getNoTagUnder6());

    }

    public long getNoElectronicTag() {
        return noElectronicTag;
    }

    private String getNoElectronicTagHtmlString() {
        return surround("noElectronicTag", getNoElectronicTag());
    }

    public long getTotalInRegistry() {
        return totalInRegistry;
    }

    private String getTotalInRegistryHtmlString() {
        return surround("totalInRegistry", getTotalInRegistry());
    }

    public long getSingleTag() {
        return singleTag;
    }

    private String getSingleTagHtmlString() {
        return surround("singleTag", getSingleTag());
    }

    public long getOver6NoTagsAndNoElectronicTags() {
        return over6NoTagsAndNoElectronicTags;
    }

    private String getOver6NoTagsAndNoElectronicTagsHtmlString() {
        return surround("getOver6NoTagsAndNoElectronicTags", getOver6NoTagsAndNoElectronicTags());
    }

    public long getCountedButNotInRegistry() {
        return countedButNotInRegistry;
    }

    private String getCountedButNotInRegistryHtmlString() {
        return surround("countedButNotInRegistry", getCountedButNotInRegistry());
    }

    public Map<AnimalType, Long> getSelectable() {
        return new HashMap<>(selectable);
    }

    public Map<AnimalType, String> getSelectableHtmlString() {
        return selectable.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                e -> surround(e.getKey().title,e.getValue())
        ));
    }

    @Override
    public String toString() {
        String hasOver20PctString = hasOver20PctMap.isEmpty() ? "" : "Ποσοστό >20% Μονόν Ενωτίων στα ακόλουθα ζώα: " + "\n" +
                hasOver20PctMap.entrySet().stream()
                        .filter(entry -> entry.getValue().booleanValue() == true)
                        .map(entry -> entry.getKey().title).collect(Collectors.joining("\n"));

        return "Παραγωγός: " + inspectee.getName() + "-" + inspectee.getTin() + "\n" +
                "Καταμετρηθέντα ζώα: " + getTotal() + "\n" +
                "Χωρίς νόμιμη σήμανση: " + getNoLegalTag() + "\n" +
                "Με νόμιμη σήμανση που δεν αναγράφονται στο μητρώο " + getCountedButNotInRegistry() + "\n" +
                "Μετά το 2010 Χωρίς ηλ. σήμανση: " + getNoElectronicTag() + "\n" +
                "> 6 μηνών με ένα ενώτιο: " + getSingleTag() + "\n" +
                "> 6 μηνών με χωρίς σήμανση (ούτε ηλεκτρονική): " + getOver6NoTagsAndNoElectronicTags() + "\n" +
                "< 6 μηνων χωρίς σήμανση: " + getNoTagUnder6() + "\n" +
                "Συνολικά αναγραφόμενα στο μητρώο " + getTotalInRegistry() + "\n" +
                "Επιλέξιμα: " + "\n" +
                selectable.entrySet().stream().map(entry -> entry.getKey().title + ":" + entry.getValue()).collect(Collectors.joining("\n")) +
                hasOver20PctString
                ;
    }

    public String getOver20PctResult() {
        return hasOver20PctMap.entrySet().stream()
                .filter(entry -> entry.getValue().booleanValue() == true)
                .map(entry -> entry.getKey().title).collect(Collectors.joining(" - "));
    }

    public String getOver20PctResultHtmlString() {
        return surround("cross-check", getOver20PctResult());
    }

    public String toHtmlString() {
        String emptyCell = "<td></td>";

        return "<h4 class=\"header\" id=\"" + inspectee.getTin() + "\">" + inspectee.getName() + " ΑΦΜ " + inspectee.getTin() + "</h4>" +
                "<b>Επιλέξιμα</b>" +
                "<table>" +
                "<tr>"
                + "<td>Αμνοερίφια με νόμιμη: <i>"
                + surround("Αμνοερίφια", selectable.entrySet().stream().filter(entry -> entry.getKey() == AnimalType.KIDLAMB_ANIMAL)
                .map(Map.Entry::getValue).reduce(0L, Long::sum).toString()) + "</i>"
                + "</td>"
                + "<td> Χωρίς σήμανση:<i>"
                + surround("χωρίς σήμανση ", getNoTagUnder6HtmlString()) + "</i>"
                + "</td>"
                + "</tr>"
                + "<tr><td>" +
                selectable.entrySet().stream().filter(entry -> entry.getKey() == AnimalType.SHEEP_ANIMAL
                        || entry.getKey() == AnimalType.GOAT_ANIMAL)
                        .map(entry -> entry.getKey().title + ": <i>" + surround(entry.getKey().title, entry.getValue()) + "</i>")
                        .collect(Collectors.joining("</td><td>"))
                + "</td></tr><tr><td> Κριοί-Τράγοι: <i> " +
                surround("Κριοί-Τράγοι", selectable.entrySet().stream().filter(entry -> entry.getKey() == AnimalType.RAM_ANIMAL
                        || entry.getKey() == AnimalType.HEGOAT_ANIMAL)
                        .map(Map.Entry::getValue).reduce(0L, Long::sum).toString()) + "</i></td>"
                + "<td>" +
                selectable.entrySet().stream().filter(entry -> entry.getKey() == AnimalType.HORSE_ANIMAL)
                        .map(entry -> entry.getKey().title + ": <i>" +
                                surround(entry.getKey().title, entry.getValue()) + "</i>").collect(Collectors.joining("</td><td>"))
                + "</td></tr>"
                + "</table>"
                + "<table>" +
                "<tr>" +
                "<td>Καταμετρηθέντα: <i>" + getTotalHtmlString() + "</i></td>"
                + "<td>Αναγραφόμενα στο μητρώο: <i>" + getTotalInRegistryHtmlString() + "</i></td>"
                + "</tr><tr>"
                + "<td> Χωρίς νόμιμη σήμανση: <i>" + getNoLegalTagHtmlString() + "</i></td>"
                + "<td> Με νόμιμη σήμανση που δεν αναγράφονται στο μητρώο: <i> " + getCountedButNotInRegistryHtmlString() + "</i></td>"
                + "</tr>"
                + "</table>"
                + "<b>Πολλαπλή συμμόρφωση</b>" +
                "<table><tr>"
                + "<td> Μετά το 2010 χωρίς ηλ. σήμανση: <i>" + getNoElectronicTagHtmlString() + "</i></td>"
                + "<td>&gt; 6 μηνών με ένα ενώτιο: <i>" + getSingleTagHtmlString() + "</i></td>"
                + "</tr><tr>"
                + "<td>&gt; 6 μηνών χωρίς ενώτια ούτε ηλ. σήμανση: <i>" + getOver6NoTagsAndNoElectronicTagsHtmlString() + "</i></td>"
                + emptyCell
                + "</tr>"
                + "</table>"
                +
                (hasOver20PctMap.isEmpty() ? "" :
                        "<b>Παρατηρήσεις</b>" +
                                "<table><tr>" +
                                "<td>Ποσοστό &gt;20% ενωτίων είναι μονά:</td>"
                                + "<td>" + getOver20PctResultHtmlString() + "</td>" +
                                "</tr>"
                                + "</table>")
                ;
    }
}
