/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import org.slf4j.Logger;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.domain.ScannedDocument;
import gr.petalidis.datamars.inspections.dto.InspectionDateProducer;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.ExcelService;
import gr.petalidis.datamars.inspections.service.FileService;
import gr.petalidis.datamars.inspections.service.InspectionService;

class CreateInspectionAdapter extends ArrayAdapter<InspectionDateProducer> {
    private static final String INSPECTION_KEY = "inspection";
    private final Context context;
    private final List<InspectionDateProducer> objects;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final DbHandler dbHandler;
    private static final Logger log = Log4jHelper.getLogger(CreateInspectionAdapter.class.getName());

    CreateInspectionAdapter(Context context, int resource, List<InspectionDateProducer> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        dbHandler = new DbHandler(context.getApplicationContext());
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = Objects.requireNonNull(inflater).inflate(R.layout.listinspections, parent, false);
        }
        final InspectionDateProducer item = objects.get(position);

        TextView inspectionDateText = rowView.findViewById(R.id.inspectionDate);
        TextView inspectionProducer = rowView.findViewById(R.id.inspectionProducer);
        CheckBox uploadButton = rowView.findViewById(R.id.uploadButton);
        uploadButton.setVisibility(View.GONE);
        //  uploadButton.setOnClickListener(l -> uploadButton.toggle());

        ImageButton exportCsvButton = rowView.findViewById(R.id.exportCsv);

        exportCsvButton.setOnClickListener(x ->
        {
            try {
                log.error("Starting export");
                File downloads = new File(context.getExternalFilesDir("").getAbsolutePath() + File.separator + "export");
                log.error("downloads is {}", downloads);
                Inspection inspection = InspectionService.findInspectionFor(dbHandler, item.getId());
                log.error("Located inspection");
                Date date = new Date();
                String randomString = inspection.getProducer1Tin() + "-" + date.getTime();
                String directory = downloads.getAbsolutePath() + File.separator + randomString;
                log.error("Starting export to {}", directory);
                FileService.copyFilesToDirectory(inspection.getScannedDocuments().stream().map(ScannedDocument::getImagePath).collect(Collectors.toList()), directory);
                log.error("Files  copied to {}", directory);
                ExcelService.export(inspection, directory, inspection.getProducer1Tin() + "-" + simpleDateFormat.format(inspection.getDate()) + ".xls");
                log.error("Excel copied to {}", directory);
                AlertDialog.Builder preSaveBuilder = new AlertDialog.Builder(this.context);

            String msg = "Τα στοιχεία αποθηκεύθηκαν στη θέση " + downloads.getAbsolutePath().replace("/storage/emulated/0", "") + File.separator + randomString;
            preSaveBuilder.setTitle("Αποθήκευση ελέγχου").setMessage(msg)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        //Do nothing... (but why?)
                    });
            preSaveBuilder.show();
        } catch (Exception e) {
            log.error("CreateInspectionActivity {}", e.getLocalizedMessage());
        }});

        if (!item.isFinalised()) {
            inspectionProducer.setTypeface(null, Typeface.ITALIC);
            inspectionDateText.setTypeface(null, Typeface.ITALIC);
            int color = ContextCompat.getColor(
                    getContext(),
                    R.color.colorAccentDark
            );
            inspectionDateText.setTextColor(color);
            inspectionProducer.setTextColor(color);
        } else {
            inspectionProducer.setTypeface(null, Typeface.NORMAL);
            inspectionDateText.setTypeface(null, Typeface.NORMAL);
            int color = ContextCompat.getColor(
                    getContext(),
                    android.R.color.black
            );
            inspectionDateText.setTextColor(color);
            inspectionProducer.setTextColor(color);
        }


        inspectionProducer.setText(item.getMainProducer());
        inspectionDateText.setText(item.getInspectionDate());
        inspectionDateText.setOnLongClickListener(x -> deleteInspectionActivity(item));
        inspectionProducer.setOnLongClickListener(x -> deleteInspectionActivity(item));

        View.OnClickListener onClickListener = x -> startInspectionActivity(item.getId());
        inspectionDateText.setOnClickListener(onClickListener);
        inspectionProducer.setOnClickListener(onClickListener);
        return rowView;
    }

    private boolean deleteInspectionActivity(InspectionDateProducer item) {
        AlertDialog.Builder preSaveBuilder = new AlertDialog.Builder(this.context);

        preSaveBuilder.setTitle("Διαγραφή ελέγχου")
                .setMessage("Είστε σίγουροι; Θα διαγραφεί ο έλεγχος του "
                        + item.getMainProducer() + " ημ/νιας " + item.getInspectionDate())
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                            InspectionService.delete(dbHandler, item.getId());
                            try {
                                List<InspectionDateProducer> inspectionDateProducerList
                                        = InspectionService.findAllInspections(dbHandler);
                                objects.clear();
                                objects.addAll(inspectionDateProducerList);
                                notifyDataSetChanged();
                            } catch (PersistenceException e) {
                                log.error("Unable to update list of inspections {} ", e.getLocalizedMessage());
                            }
                        }
                )
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    //do nothing
                });
        preSaveBuilder.show();
        return true;
    }

    private boolean startInspectionActivity(String id) {
        try {
            Inspection inspection = InspectionService.findInspectionFor(dbHandler, id);

            Intent intent = inspection.isFinalised()
                    ? new Intent(context, InspectionViewActivity.class)
                    : new Intent(context, InspectionStepTwoActivity.class);
            intent.putExtra(INSPECTION_KEY, inspection);
            context.startActivity(intent);
            return true;
        } catch (PersistenceException e) {
            log.error("CreateInspectionAdapter: {}", e.getLocalizedMessage());
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void selectInspectionsToSend(ListView listview) {
        for (int i = 0; i < getCount(); i++) {
            InspectionDateProducer item = getItem(i);
            View view;
            final int firstListItemPosition = listview.getFirstVisiblePosition();
            final int lastListItemPosition = firstListItemPosition + listview.getChildCount() - 1;

            if (i < firstListItemPosition || i > lastListItemPosition) {
                view = getView(i, null, listview);
            } else {
                final int childIndex = i - firstListItemPosition;
                view = listview.getChildAt(childIndex);
            }
            view.findViewById(R.id.uploadButton).setVisibility(item.canBeUploaded() ? View.VISIBLE : View.INVISIBLE);
        }

    }

    public void resetSelectedInspections(ListView listview) {
        for (int i = 0; i < getCount(); i++) {
            View view;
            final int firstListItemPosition = listview.getFirstVisiblePosition();
            final int lastListItemPosition = firstListItemPosition + listview.getChildCount() - 1;

            if (i < firstListItemPosition || i > lastListItemPosition) {
                view = getView(i, null, listview);
            } else {
                final int childIndex = i - firstListItemPosition;
                view = listview.getChildAt(childIndex);
            }
            view.findViewById(R.id.uploadButton).setVisibility(View.GONE);
        }

    }


    public List<InspectionDateProducer> getSelectedInspections(ListView listview) {
        List<InspectionDateProducer> inspectionDateProducers = new ArrayList<>();
        for (int i = 0; i < getCount(); i++) {
            InspectionDateProducer item = getItem(i);
            View view;
            final int firstListItemPosition = listview.getFirstVisiblePosition();
            final int lastListItemPosition = firstListItemPosition + listview.getChildCount() - 1;

            if (i < firstListItemPosition || i > lastListItemPosition) {
                view = getView(i, null, listview);
            } else {
                final int childIndex = i - firstListItemPosition;
                view = listview.getChildAt(childIndex);
            }
            CheckBox viewById = view.findViewById(R.id.uploadButton);
            if (viewById.isChecked()) {
                inspectionDateProducers.add(item);
            }
        }
        return inspectionDateProducers;
    }
}
