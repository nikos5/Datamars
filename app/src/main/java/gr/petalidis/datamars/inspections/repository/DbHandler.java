/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.slf4j.Logger;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;

public class DbHandler extends SQLiteOpenHelper {
    static final String TABLE_INSPECTIONS = "INSPECTIONS";
    static final String TABLE_INSPECTION_ENTRIES = "ENTRIES";
    static final String TABLE_INSPECTION_OTHER_ENTRIES = "OTHER_ENTRIES";
    static final String TABLE_INSPECTION_SCANNED_DOCUMENT = "SCANNED_DOCUMENT";
    static final String TABLE_USB_ALIAS = "USB_ALIAS";
    private static final Logger log = Log4jHelper.getLogger(DbHandler.class.getName());
    // Database Version
    private static final int DATABASE_VERSION = 10;

    public DbHandler(Context context) {
        super(context, Moo.getProperty("database"),
                null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE_INSPECTIONS = "CREATE TABLE IF NOT EXISTS " + TABLE_INSPECTIONS +
                " ( " +
                "id TEXT PRIMARY KEY, " +
                "producer1Tin TEXT, " +
                "producer1Name TEXT, " +
                "producer2Tin TEXT, " +
                "producer2Name TEXT, " +
                "producer3Tin TEXT, " +
                "producer3Name TEXT, " +
                "producer4Tin TEXT, " +
                "producer4Name TEXT, " +
                "locationX REAL," +
                "locationY REAL," +
                "date TEXT, " +
                "uploaded INTEGER DEFAULT 0 NOT NULL, " +
                "finalised INTEGER, " +
                "totalInRegistryForProducer1 INTEGER, " +
                "totalInRegistryForProducer2 INTEGER, " +
                "totalInRegistryForProducer3 INTEGER, " +
                "totalInRegistryForProducer4 INTEGER" +
                ") ";
        String CREATE_TABLE_INSPECTION_ENTRIES = "CREATE TABLE IF NOT EXISTS " + TABLE_INSPECTION_ENTRIES +
                " ( " +
                "id TEXT PRIMARY KEY, " +
                "inspectionId TEXT, " +
                "country TEXT, " +
                "tag TEXT, " +
                "tagDate TEXT, " +
                "isInRegister INTEGER, " +
                "producer TEXT, " +
                "producerTin TEXT, " +
                "animalType TEXT, " +
                "animalGenre TEXT, " +
                "comment TEXT, " +
                "isAutomatic INTEGER DEFAULT 1 NOT NULL" +
                ") ";

        String CREATE_TABLE_INSPECTION_OTHER_ENTRIES = "CREATE TABLE IF NOT EXISTS " + TABLE_INSPECTION_OTHER_ENTRIES +
                " ( " +
                "id TEXT PRIMARY KEY, " +
                "type TEXT, " +
                "inspectionId TEXT, " +
                "producerTin TEXT, " +
                "producerName TEXT, " +
                "animal TEXT, " +
                "value INTEGER" +
                ")";

        String CREATE_TABLE_INSPECTION_IMAGES = "CREATE TABLE IF NOT EXISTS " + TABLE_INSPECTION_SCANNED_DOCUMENT +
                " ( " +
                "id TEXT PRIMARY KEY, " +
                "inspectionId TEXT, " +
                "imagePath TEXT " +
                ") ";

        String CREATE_TABLE_USB_ALIAS = "CREATE TABLE IF NOT EXISTS " + TABLE_USB_ALIAS +
                " ( " +
                "usb TEXT PRIMARY KEY, " +
                "alias TEXT " +
                ") ";
        sqLiteDatabase.execSQL(CREATE_TABLE_INSPECTIONS);
        sqLiteDatabase.execSQL(CREATE_TABLE_INSPECTION_ENTRIES);
        sqLiteDatabase.execSQL(CREATE_TABLE_INSPECTION_OTHER_ENTRIES);
        sqLiteDatabase.execSQL(CREATE_TABLE_INSPECTION_IMAGES);
        sqLiteDatabase.execSQL(CREATE_TABLE_USB_ALIAS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion < 4) {
            String ADD_FINALISED_TO_INSEPECTIONS = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD finalised INTEGER DEFAULT 1 NOT NULL";

            try {
                sqLiteDatabase.execSQL(ADD_FINALISED_TO_INSEPECTIONS); //
            } catch (Exception e) {
                log.error("Could not add finalised field from {} to {}", oldVersion, newVersion);
            }

        } else if (oldVersion >= 4 && oldVersion < 10) {
            String ADD_UPLOADED_TO_INSPECTIONS = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD uploaded INTEGER DEFAULT 0 NOT NULL";

            try {
                sqLiteDatabase.execSQL(ADD_UPLOADED_TO_INSPECTIONS); //
            } catch (Exception e) {
                log.error("Could not add uploaded field from {} to {}", oldVersion, newVersion);
            }

            String ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS1 = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD totalInRegistryForProducer1 INTEGER DEFAULT 0 NOT NULL";
            String ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS2 = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD totalInRegistryForProducer2 INTEGER DEFAULT 0 NOT NULL";
            String ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS3 = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD totalInRegistryForProducer3 INTEGER DEFAULT 0 NOT NULL";
            String ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS4 = "ALTER TABLE " + TABLE_INSPECTIONS + "  \n" +
                    "\tADD totalInRegistryForProducer4 INTEGER DEFAULT 0 NOT NULL";

            try {
                sqLiteDatabase.execSQL(ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS1); //
                sqLiteDatabase.execSQL(ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS2); //
                sqLiteDatabase.execSQL(ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS3); //
                sqLiteDatabase.execSQL(ADD_TOTAL_IN_REGISTER_TO_INSPECTIONS4); //
            } catch (Exception e) {
                log.error("Could not add totalInRegistryForProducer1-4 field from {} to {}", oldVersion, newVersion);
            }

            String ADD_IS_AUTOMATIC_TO_ENTRIES = "ALTER TABLE " + TABLE_INSPECTION_ENTRIES + "  \n" +
                    "\tADD isAutomatic INTEGER DEFAULT 1 NOT NULL";
            try {
                sqLiteDatabase.execSQL(ADD_IS_AUTOMATIC_TO_ENTRIES); //

            } catch (Exception e) {
                log.error("Could not add field isAutomatic from {} to {}", oldVersion, newVersion);
            }
        } else {
            onCreate(sqLiteDatabase);
        }
    }


    public void dropDatabase(Context context) {
        context.deleteDatabase(this.getDatabaseName());
    }

}
