/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import org.slf4j.Logger;

import java.util.Map;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.AnimalType;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.domain.Report;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;

public class InspectionViewActivity extends AppCompatActivity implements GpsFragment.GpsUpdateLocation {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private static final String INSPECTION_KEY = "inspection";
    private static final Logger log = Log4jHelper.getLogger(InspectionViewActivity.class.getName());
    private Inspection inspection;
    private int index = 0;
    private double longitude;
    private double latitude;

    @Override
    public void setLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        ImageButton saveButton = findViewById(R.id.saveButton);
        saveButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            inspection = (Inspection) savedInstanceState.getSerializable(INSPECTION_KEY);
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable(INSPECTION_KEY);
        }
        setContentView(R.layout.activity_inspection_view);

        DbHandler dbHandler = new DbHandler(Moo.getAppContext());
        try {
            inspection = InspectionService.findInspectionFor(dbHandler, inspection.getId().toString());

            setInspectionDateTextView();
            updateButtons();

            GpsFragment gpsFragment = (GpsFragment) getSupportFragmentManager().findFragmentById(R.id.gpsFragment);

            gpsFragment.setText(inspection.getLatitude(), inspection.getLongitude(), 0.0f);

            ImageButton saveButton = findViewById(R.id.saveButton);
            saveButton.setVisibility(View.INVISIBLE);
            saveButton.setOnClickListener(l -> {
                if (inspection != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                                //Do Nothing
                            });
                    try {
                        inspection.setLongitude(longitude);
                        inspection.setLatitude(latitude);
                        InspectionService.updateLocation(dbHandler, inspection.getId(), latitude, longitude);

                        builder.setTitle(R.string.success).setMessage(R.string.success);
                        builder.show();
                    } catch (PersistenceException e) {
                        log.error("Αποτυχία αποθήκευσης ελέγχου {}. Λόγος {}", inspection.getProducer1Tin(), e.getMessage());
                        builder.setTitle(R.string.failure).setMessage(e.getMessage());
                        builder.show();
                    }
                }

                saveButton.setVisibility(View.INVISIBLE);
                gpsFragment.turnOffGps();
            });
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (PersistenceException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                        //Do Nothing
                    });
            log.error("Unable to read inspection for {}. Reason: {}", inspection.getProducer1Name(), e.getMessage());
            builder.setTitle(R.string.failure).setMessage(e.getMessage());
            builder.show();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INSPECTION_KEY, inspection);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setInspectionDateTextView() {
        TextView inspectionDateTextView = findViewById(R.id.viewInspectionDateValue);
        if (inspection != null)
            inspectionDateTextView.setText(dateFormat.format(inspection.getDate()));
    }

    public void goToViewActivityStep2(View view) {
        Intent intent = new Intent(this, InspectionViewActivity2.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        startActivity(intent);
    }

    public void viewPhotos(View view) {
        Intent intent = new Intent(this, InspectionViewPhotoActivity.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        startActivity(intent);
    }

    public void goToPreviousProducer(View view) {
        if (index > 0) {
            index--;
        }
        updateButtons();
    }

    public void goToNextProducer(View view) {
        if (index < inspection.getProducersWithNoDummy().size() - 1) {
            index++;
        }

        updateButtons();
    }

    private void updateButtons() {
        if (inspection != null) {
            Inspectee inspectee = inspection.getProducersWithNoDummy().get(index);

            if (inspectee != null) {
                TextView inspecteeView = findViewById(R.id.viewProducerTin);
                inspecteeView.setText(inspectee.getTin() + " " +
                        inspectee.getName());

                Report report = inspection.generateReportFor(inspectee);

                TextView totalValue = findViewById(R.id.total);
                totalValue.setText(report.getTotal() + "");

                TextView totalInRegistry = findViewById(R.id.totalInRegistryValue);
                totalInRegistry.setText(report.getTotalInRegistry() + "");

                TextView noTag = findViewById(R.id.noTag);
                noTag.setText(report.getNoLegalTag() + "");

                TextView countedButNotInRegistry = findViewById(R.id.countedButNotInRegistry);
                countedButNotInRegistry.setText(report.getCountedButNotInRegistry() + "");

                TextView noElectronicTag = findViewById(R.id.noElectronicTag);
                noElectronicTag.setText(report.getNoElectronicTag() + "");

                TextView singleTag = findViewById(R.id.singleTag);
                singleTag.setText(report.getSingleTag() + "");

                TextView getOver6NoTagsAndNoElectronicTags = findViewById(R.id.getOver6NoTagsAndNoElectronicTags);
                getOver6NoTagsAndNoElectronicTags.setText(report.getOver6NoTagsAndNoElectronicTags() + "");

                TextView noTagUnder6 = findViewById(R.id.noTagUnder6);
                noTagUnder6.setText(report.getNoTagUnder6() + "");

                Map<AnimalType, Long> selectable = report.getSelectable();
                TextView sheepTotalValue = findViewById(R.id.sheepTotalValue);
                sheepTotalValue.setText(selectable.get(AnimalType.SHEEP_ANIMAL) + "");
                TextView goatTotalValue = findViewById(R.id.goatTotalValue);
                goatTotalValue.setText(selectable.get(AnimalType.GOAT_ANIMAL) + "");

                TextView ramTotalValue = findViewById(R.id.ramTotalValue);
                ramTotalValue.setText(selectable.get(AnimalType.RAM_ANIMAL) + selectable.get(AnimalType.HEGOAT_ANIMAL) + "");

                TextView lambsTotalValue = findViewById(R.id.lambsTotalValue);
                lambsTotalValue.setText(selectable.get(AnimalType.KIDLAMB_ANIMAL) + "");

                TextView horseTotalValue = findViewById(R.id.horseTotalValue);
                horseTotalValue.setText(selectable.get(AnimalType.HORSE_ANIMAL) + "");
                if (!report.getOver20PctResult().isEmpty()) {
                    TextView warningsLabel = findViewById(R.id.warningsLabel);
                    warningsLabel.setText(">20% μονά ενώτια στα ακόλουθα ζώα:");
                }
                TextView warning = findViewById(R.id.warnings);
                warning.setText(report.getOver20PctResult());

                ImageButton previousProducerButton = findViewById(R.id.previousProducer3);
                ImageButton nextProducerButton = findViewById(R.id.nextProducer3);

                if (inspection.getProducersWithNoDummy().isEmpty() || inspection.getProducersWithNoDummy().size() == 1) {
                    previousProducerButton.setClickable(false);
                    nextProducerButton.setClickable(false);
                } else {
                    if (index == 0) {
                        previousProducerButton.setClickable(false);
                        nextProducerButton.setClickable(true);
                    } else if (index == inspection.getProducersWithNoDummy().size() - 1) {
                        nextProducerButton.setClickable(false);
                        previousProducerButton.setClickable(true);
                    } else {
                        previousProducerButton.setClickable(true);
                        nextProducerButton.setClickable(true);
                    }
                }
            } else {
                log.error("InspectionViewActivity: Tried to get inspectee with index {} for inspection {} but was null", index,
                        inspection.getId());

            }
        } else {
            log.error("InspectionViewActivity: Tried to get inspection but was null");
        }
    }
}
