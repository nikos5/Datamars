/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.slf4j.Logger;

import java.util.Objects;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;

public class InspectionStepFiveActivity extends AppCompatActivity {
    private static final String INSPECTION_KEY = "inspection";
    private static final String INSPECTEE_KEY = "inspectee";

    private Inspection inspection;
    private Inspectee inspectee;

    private static final Logger log = Log4jHelper.getLogger(InspectionStepFiveActivity.class.getName());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // recovering the instance state
        if (savedInstanceState != null) {
            inspection = (Inspection) savedInstanceState.getSerializable(INSPECTION_KEY);
            inspectee = (Inspectee) savedInstanceState.getSerializable(INSPECTEE_KEY);
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable(INSPECTION_KEY);
            inspectee = (Inspectee) getIntent().getExtras().getSerializable(INSPECTEE_KEY);
        }

        setContentView(R.layout.activity_inspection_step_five);
        updateButtons();

        TextView producerLabel = findViewById(R.id.producerLabel);
        producerLabel.setText(inspectee.toString());
        TextView value = findViewById(R.id.noTagUnder6MonthValue);
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    inspection.setNoTagUnder6Month(inspectee,
                            Integer.parseInt(editable.toString()));
                }
            }
        });
        value = findViewById(R.id.noTagOver6MonthValue);
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    inspection.setNoTagOver6Month(inspectee,
                            Integer.parseInt(editable.toString()));
                }
            }
        });

        value = findViewById(R.id.noTagUnder6MonthValueOutOfRegistry);
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    inspection.setNoTagUnder6MonthOutOfRegistry(inspectee,
                            Integer.parseInt(editable.toString()));
                }
            }
        });
        value = findViewById(R.id.totalAnimalsInRegistryValue);
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    inspection.setTotalInRegistryForProducer(inspectee.getTin(), Integer.parseInt(editable.toString()));
                }
            }
        });
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INSPECTION_KEY, inspection);
        outState.putSerializable(INSPECTEE_KEY, inspectee);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.temp_save:

                if (inspection != null) {
                    DbHandler dbHandler = new DbHandler(Moo.getAppContext());
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                                //Do Nothing
                            });
                    try {
                        inspection = InspectionService.save(dbHandler, inspection);
                        builder.setTitle(R.string.success).setMessage(R.string.success);
                        builder.show();
                    } catch (PersistenceException e) {
                        builder.setTitle(R.string.failure).setMessage(e.getMessage());
                        builder.show();
                    }
                }
                return true;
            case R.id.about:
                PackageManager packageManager = this.getPackageManager();
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                            //Do Nothing
                        });
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                    String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                            "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                            "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                    builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                    builder.show();
                } catch (PackageManager.NameNotFoundException e) {
                    log.error("Could not show about message {}", e.getLocalizedMessage());
                }

                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void gotoInspectionCohabitants(View view) {

        Intent intent = new Intent(this, InspectionCohabitants.class);

        intent.putExtra(INSPECTION_KEY, inspection);

        startActivity(intent);
    }

    private void updateButtons() {

        TextView noTagOver6MonthOld = findViewById(R.id.noTagOver6MonthValue);
        noTagOver6MonthOld.setText(inspection.getNoTagOver6MonthOld(inspectee) + "");
        TextView noTagUnder6MonthOld = findViewById(R.id.noTagUnder6MonthValue);
        noTagUnder6MonthOld.setText(inspection.getNoTagUnder6MonthOld(inspectee) + "");
        TextView noTagUnder6MonthOldOutOfRegistry = findViewById(R.id.noTagUnder6MonthValueOutOfRegistry);
        noTagUnder6MonthOldOutOfRegistry.setText(inspection.getNoTagUnder6MonthOldOutOfRegistry(inspectee) + "");
        TextView totalAnimalsInRegistryValue = findViewById(R.id.totalAnimalsInRegistryValue);
        totalAnimalsInRegistryValue.setText(inspection.getTotalInRegistryForProducer(inspectee) + "");

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InspectionStepFourActivity.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        intent.putExtra(INSPECTEE_KEY, inspectee);
        startActivity(intent);
    }
}
