/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.UUID;

import gr.petalidis.datamars.inspections.domain.CommentType;
import gr.petalidis.datamars.inspections.domain.Entry;
import gr.petalidis.datamars.inspections.dto.util.UUIDGenerator;

public class EntryDto implements Serializable {
    @JsonIgnore
    final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private UUID id = UUIDGenerator.generate();

    private InspecteeDto inspectee = new InspecteeDto("", "");

    private String country = "";

    private String tag = "";

    private String tagDate;

    private boolean isInRegister = true;

    private AnimalType animalType = AnimalType.SHEEP_ANIMAL;

    private AnimalGenre animalGenre = AnimalGenre.NONE_SHEEP;

    private CommentType comment = CommentType.EMPTY;

    private boolean automatic = true;

    public EntryDto() {

    }

    public EntryDto(Entry entry) {
        this.id = entry.getId();
        this.inspectee = new InspecteeDto(entry.getProducerTin(), entry.getProducer());
        this.country = entry.getCountry();
        this.tag = entry.getTag();
        this.isInRegister = entry.isInRegister();

        this.tagDate = format.format(entry.getTagDate());

        this.animalType = AnimalType.fromString(entry.getAnimalType());
        this.animalGenre = AnimalGenre.getGenreFromType(
                this.animalType, entry.getAnimalGenre());
        this.comment = entry.getComment();
        this.automatic = entry.isAutomatic();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    @JsonIgnore
    public String getOwner() {
        return tag.substring(4, 8);
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTagDate() {
        return tagDate;
    }

    public void setTagDate(String tagDate) {
        this.tagDate = tagDate;
    }

    public boolean isInRegister() {
        return isInRegister;
    }

    public void setInRegister(boolean inRegister) {
        isInRegister = inRegister;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public AnimalGenre getAnimalGenre() {
        return animalGenre;
    }

    public void setAnimalGenre(AnimalGenre animalGenre) {
        this.animalGenre = animalGenre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public CommentType getComment() {
        return comment;
    }

    public void setComment(CommentType comment) {
        this.comment = comment;
    }


    public InspecteeDto getInspectee() {
        return inspectee;
    }

    public void setInspectee(InspecteeDto inspectee) {
        this.inspectee = inspectee;
    }

    public static SimpleDateFormat getFormat() {
        return format;
    }

    public boolean isAutomatic() {
        return automatic;
    }

    public void setAutomatic(boolean automatic) {
        this.automatic = automatic;
    }
}
