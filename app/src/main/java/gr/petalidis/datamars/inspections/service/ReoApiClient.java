/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;


import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.slf4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.inspections.dto.inspection.InspectionDto;
import gr.petalidis.datamars.inspections.dto.inspection.InspectionDtoDeserializer;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ReoApiClient {
    static final String BASE_URL = Moo.getProperty("server");
    private static final Logger log = Log4jHelper.getLogger(LoginApiClient.class.getName());

    public static Boolean postInspection(String jwt, InspectionDto inspectionDto) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module =
                new SimpleModule("InspectionDtoDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(InspectionDto.class, new InspectionDtoDeserializer());
        // objectMapper.registerModule(module);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .client(okHttpClient)
                .build();

        ReoApi reoApi = retrofit.create(ReoApi.class);


        Call<InspectionDto> call = reoApi.postInspection(jwt, inspectionDto);

        Response<InspectionDto> response = call.execute();

        if (response.isSuccessful()) {
            return true;
        } else {
            log.error("Tried to post inspection with tablet id {}. Got back {}", inspectionDto.getTabletId(), response.code());
            throw new IOException("Got back error code: " + response.code());
        }


    }
}