/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

public class UploadResult {
    private boolean success = false;
    private String reason = "";

    public UploadResult(boolean success, String reason) {
        this.success = success;
        this.reason = reason;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getReason() {
        return reason;
    }

    //    public void onSuccess(T data) {
//           success = true;
////            Toast.makeText( activityRef.get().getApplicationContext(),
////                    "Οι έλεγχοι μεταφορτώθηκαν επιτυχώς",
////                    Toast.LENGTH_SHORT).show();
//
//    }
//    public void onError(Exception exception) {
//        success = false;
//        reason = exception.getLocalizedMessage();
//
////            Toast.makeText( activityRef.get().getApplicationContext(),
////                    "Οι έλεγχοι δε μεταφορτώθηκαν!  " + exception.getLocalizedMessage(),
////                    Toast.LENGTH_SHORT).show();
//    };
}
