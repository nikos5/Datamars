/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public enum AnimalGenre {
    NONE_GOAT(AnimalType.GOAT_ANIMAL, "Όχι σπάνια"),
    SKOPELOS_GOAT(AnimalType.GOAT_ANIMAL, "Σκοπέλου"),
    NONE_SHEEP(AnimalType.SHEEP_ANIMAL, "Όχι σπάνια"),
    KATAFYGIOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Καταφυγίου"),
    ROUMLOUKIOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Ρουμλουκίου"),
    HPEIROU_SHEEP(AnimalType.SHEEP_ANIMAL, "Ορεινό Ηπείρου"),
    IKARIAS_SHEEP(AnimalType.SHEEP_ANIMAL, "Ικαρίας"),
    LEYKIMHS_SHEEP(AnimalType.SHEEP_ANIMAL, "Λευκίμης"),
    KATSIKA_SHEEP(AnimalType.SHEEP_ANIMAL, "Κατσικά"),
    ARGOUS_SHEEP(AnimalType.SHEEP_ANIMAL, "Άργους"),
    ZAKYNTHOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Ζακύνθου"),
    FLORINAS_SHEEP(AnimalType.SHEEP_ANIMAL, "Φλώρινας - Πελαγονίας"),
    SARAKATSANIKO_SHEEP(AnimalType.SHEEP_ANIMAL, "Σαρακατσάνικο"),
    SKOPELOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Γλώσσας Σκοπέλου"),
    KYMHS_SHEEP(AnimalType.SHEEP_ANIMAL, "Κύμης"),
    AGRINIOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Αγρινίου"),
    DRAMAS_SHEEP(AnimalType.SHEEP_ANIMAL, "Δράμας"),
    THRAKIS_SHEEP(AnimalType.SHEEP_ANIMAL, "Θράκης"),
    KALARRYTIKO_SHEEP(AnimalType.SHEEP_ANIMAL, "Καλαρρύτικο"),
    PILIOU_SHEEP(AnimalType.SHEEP_ANIMAL, "Πηλίου"),
    SERRON_SHEEP(AnimalType.SHEEP_ANIMAL, "Σερρών"),
    NONE_HORSE(AnimalType.HORSE_ANIMAL, "Όχι σπάνια"),
    ILIAS_HORSE(AnimalType.HORSE_ANIMAL, "Ηλείας"),
    PINIAS_HORSE(AnimalType.HORSE_ANIMAL, "Πηνείας"),
    THESSALIAS_HORSE(AnimalType.HORSE_ANIMAL, "Θεσσαλίας"),
    SKIROU_HORSE(AnimalType.HORSE_ANIMAL, "Σκύρου"),
    PINDOU_HORSE(AnimalType.HORSE_ANIMAL, "Πίνδου"),
    NONE_KID(AnimalType.KID_ANIMAL, "Όχι σπάνια"),
    NONE_LAMB(AnimalType.LAMB_ANIMAL, "Όχι σπάνια"),
    NONE_HEGOAT(AnimalType.HEGOAT_ANIMAL, "Όχι σπάνια"),
    NONE_RAM(AnimalType.RAM_ANIMAL, "Όχι σπάνια"),
    KIDLAMB_ANIMAL(AnimalType.KIDLAMB_ANIMAL, "Όχι σπάνια");
    private final String name;
    private final AnimalType animalType;

    AnimalGenre(AnimalType animalType, String name) {
        this.animalType = animalType;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public static List<AnimalGenre> getTypesFor(AnimalType animalType) {
        return
                EnumSet.allOf(AnimalGenre.class).stream()
                        .filter(x -> x.getAnimalType() == animalType)
                        .collect(Collectors.toList());
    }

    public static AnimalGenre getGenreFromType(AnimalType animalType, String race) {
        switch (animalType) {
            case KID_ANIMAL:
                return NONE_KID;
            case LAMB_ANIMAL:
                return NONE_LAMB;
            case RAM_ANIMAL:
                return NONE_RAM;
            case HEGOAT_ANIMAL:
                return NONE_HEGOAT;
            case KIDLAMB_ANIMAL:
                return AnimalGenre.KIDLAMB_ANIMAL;
            case GOAT_ANIMAL:
                if (race.equals("Σκοπέλου")) {
                    return AnimalGenre.SKOPELOS_GOAT;
                } else if (race.equals("Όχι σπάνια")) {
                    return AnimalGenre.NONE_GOAT;
                }
                return AnimalGenre.NONE_GOAT;
            case SHEEP_ANIMAL:
                List<AnimalGenre> typesFor = getTypesFor(AnimalType.SHEEP_ANIMAL);
                return typesFor.stream().filter(x -> x.getName().equals(race))
                        .findFirst().orElse(AnimalGenre.NONE_SHEEP);
            case HORSE_ANIMAL:
                typesFor = getTypesFor(AnimalType.HORSE_ANIMAL);
                return typesFor.stream().filter(x -> x.getName().equals(race))
                        .findFirst().orElse(AnimalGenre.NONE_HORSE);
            default:
                return AnimalGenre.NONE_SHEEP;
        }
    }
}
