/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.dto.LoginDto;
import gr.petalidis.datamars.inspections.dto.UserDto;
import gr.petalidis.datamars.inspections.dto.inspection.DocumentDto;
import gr.petalidis.datamars.inspections.dto.inspection.InspectionDto;
import gr.petalidis.datamars.inspections.exceptions.PersistenceException;
import gr.petalidis.datamars.inspections.repository.DbHandler;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class UploadInspectionsService extends IntentService {
    private static final Logger log = Log4jHelper.getLogger(UploadInspectionsService.class.getName());

    public UploadInspectionsService() {
        super("UploadInspectionsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = new Bundle();
        if (intent != null) {
            DbHandler dbHandler = new DbHandler(getApplicationContext());
            int result = UploadResultReceiver.RESULT_CODE_OK;
            ResultReceiver resultReceiver = intent.getParcelableExtra("receiver");
            String username1 = intent.getStringExtra("username1");
            String username2 = intent.getStringExtra("username2");
            String password = intent.getStringExtra("password");
            ArrayList<String> idList = intent.getStringArrayListExtra("inspections");
            List<InspectionDto> inspectionDtoList = new ArrayList<>();
            Map<InspectionDto, Inspection> inspectionToDtoMap = new HashMap<>();
            String jwt = "";
            try {


                LoginDto loginDto = new LoginDto(username1, password);
                UserDto userDto = LoginApiClient.login(loginDto);
                jwt = userDto.getAuthenticationHeader();
                List<InspectionDto> inspectionDtos = new ArrayList<>();
                for (String id : idList) {

                    Inspection inspection = InspectionService.findInspectionFor(dbHandler, id);
                    inspectionDtos.addAll(InspectionDto.createInspections(inspection,
                            username1,
                            username2
                    ));
                }
                int totalSize = inspectionDtos.stream().map(x -> x.getScannedDocuments().stream().map(y -> y.getData().length).reduce(0, Integer::sum)).reduce(0, Integer::sum);

                totalSize += inspectionDtos.size() * 1000; //assume each inspection 1000 bytes
                int transmittedSoFar = 0;
                bundle.putFloat(UploadResultReceiver.PARAM_TOTAL_SIZE, totalSize);
                resultReceiver.send(UploadResultReceiver.RESULT_CODE_PROGRESS_START, bundle);

                for (InspectionDto dto : inspectionDtos) {
                    List<DocumentDto> documentDtos =
                            dto.getScannedDocuments().stream().map(x -> new DocumentDto(x.getDocumentId(), x.getData())).collect(Collectors.toList());
                    for (DocumentDto documentDto : documentDtos) {
                        DocumentApiClient.createDocument("Bearer " + userDto.getAuthenticationHeader(), documentDto);
                        transmittedSoFar += documentDto.getDocument().length;
                        Bundle bundle1 = new Bundle();
                        bundle1.putFloat(UploadResultReceiver.PARAM_PROGRESS, (transmittedSoFar * 100.00f / (float) totalSize));
                        resultReceiver.send(UploadResultReceiver.RESULT_CODE_PROGRESS, bundle1);

                    }
                    ReoApiClient.postInspection("Bearer " + userDto.getAuthenticationHeader(), dto);
                    InspectionService.updateUploaded(dbHandler, dto.getTabletId());
                    transmittedSoFar += 1000;
                    Bundle bundle1 = new Bundle();
                    bundle1.putFloat(UploadResultReceiver.PARAM_PROGRESS, (transmittedSoFar * 100.00f / (float) totalSize));
                    resultReceiver.send(UploadResultReceiver.RESULT_CODE_PROGRESS, bundle1);
                }

                bundle.putSerializable(UploadResultReceiver.PARAM_RESULT, true);

            } catch (IOException e) {
                log.error("Unable to upload inspection for username {}. Reason ", username1, e.getMessage());
                bundle.putSerializable(UploadResultReceiver.PARAM_EXCEPTION, e);
                result = UploadResultReceiver.RESULT_CODE_ERROR;
            } catch (PersistenceException e) {
                log.error("Unable to read inspection from database. Reason {}", e.getMessage());
                bundle.putSerializable(UploadResultReceiver.PARAM_EXCEPTION, e);
                result = UploadResultReceiver.RESULT_CODE_ERROR;
            }
            if (!jwt.isEmpty()) {
                LogoutApiClient.logout("Bearer " + jwt);
            }
            resultReceiver.send(result, bundle);
        }
    }
}
