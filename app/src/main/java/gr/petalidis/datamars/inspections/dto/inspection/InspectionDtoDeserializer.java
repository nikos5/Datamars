/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.UUID;

public class InspectionDtoDeserializer extends StdDeserializer<InspectionDto> {
    public InspectionDtoDeserializer() {
        this(null);
    }

    public InspectionDtoDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public InspectionDto deserialize(JsonParser parser, DeserializationContext deserializer) {
        InspectionDto inspectionDto = new InspectionDto();
        ObjectCodec codec = parser.getCodec();
        try {
            JsonNode node = codec.readTree(parser);

            // try catch block
            JsonNode colorNode = node.get("id");
            UUID id = UUID.fromString(colorNode.asText());
            inspectionDto.setId(id);
        } catch (IOException e) {
            e.getLocalizedMessage();
        }
        return inspectionDto;
    }
}
