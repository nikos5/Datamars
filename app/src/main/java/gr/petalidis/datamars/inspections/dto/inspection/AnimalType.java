/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;

import java.util.Arrays;
import java.util.List;

public enum AnimalType {
    KID_ANIMAL("Ερίφιο"),
    GOAT_ANIMAL("Γίδα"),
    LAMB_ANIMAL("Αρνί"),
    SHEEP_ANIMAL("Προβατίνα"),
    HEGOAT_ANIMAL("Τράγος"),
    RAM_ANIMAL("Κριάρι"),
    HORSE_ANIMAL("Άλογο"),
    KIDLAMB_ANIMAL("Αμνοερίφια");

    public final String title;

    AnimalType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public static List<String> getSheepGoatAnimals() {
        return Arrays.asList(SHEEP_ANIMAL.title, GOAT_ANIMAL.title, HEGOAT_ANIMAL.title, RAM_ANIMAL.title);
    }

    public static AnimalType fromString(String value) {
        return
                Arrays.stream(AnimalType.values()).filter(x -> x.getTitle().equals(value))
                        .findFirst().orElseThrow(() -> new IllegalArgumentException("Unknown Animal Type: " + value));
    }
}
