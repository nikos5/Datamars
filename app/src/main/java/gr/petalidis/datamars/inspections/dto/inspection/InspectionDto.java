/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.dto.util.UUIDGenerator;

public class InspectionDto implements Serializable {
    @JsonIgnore
    final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private UUID id = UUIDGenerator.generate();

    private UUID tabletId;


    private String date;

    private int year;

    private InspecteeDto inspectee;

    private Set<EntryDto> entries = new HashSet<>();

    private Set<OtherEntryDto> conventionalTags = new HashSet<>();

    private Set<DocumentRecordDto> scannedDocuments = new HashSet<>();

    private InspectorCommentDto inspector1;

    private InspectorCommentDto inspector2;

    private double latitude = 0.0f;
    private double longitude = 0.0f;

    private int totalInRegistryForProducer = 0;

    public InspectionDto() {
    }

    public static List<InspectionDto> createInspections(Inspection inspection, String username1, String username2) {

        return
                inspection.getProducersWithNoDummy().stream().map(inspectee ->
                {
                    InspectionDto inspectionDto = new InspectionDto();
                    inspectionDto.tabletId = inspection.getId();

                    inspectionDto.setYear(2019);
                    inspectionDto.setDate(format.format(inspection.getDate()));

                    InspecteeDto inspecteeDto = new InspecteeDto();
                    inspecteeDto.setTin(inspectee.getTin());
                    inspecteeDto.setName(inspectee.getName());
                    inspectionDto.setInspectee(inspecteeDto);

                    inspection.getEntriesFor(inspecteeDto.getTin()).forEach(x ->
                            inspectionDto.getEntries().add(new EntryDto(x)));


                    inspection.getConventionalTagsFor(inspecteeDto.getTin()).forEach(x ->
                            inspectionDto.getConventionalTags().add(new OtherEntryDto(x)));

                    inspectionDto.setLatitude(inspection.getLatitude());
                    inspectionDto.setLongitude(inspection.getLongitude());
                    inspectionDto.setTotalInRegistryForProducer(inspection.getTotalInRegistryForProducer(inspectee.getTin()));

                    inspectionDto.setScannedDocuments(
                            inspection.getScannedDocuments().stream().map(x ->
                                    new DocumentRecordDto(x)).collect(Collectors.toSet()));
                    InspectorCommentDto commentDto = new InspectorCommentDto();
                    commentDto.setUsername(username1.trim());
                    inspectionDto.setInspector1(commentDto);

                    InspectorCommentDto commentDto2 = new InspectorCommentDto();
                    commentDto2.setUsername(username2.trim());
                    inspectionDto.setInspector2(commentDto2);
                    return inspectionDto;
                }).collect(Collectors.toList());

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTabletId() {
        return tabletId;
    }

    public void setTabletId(UUID tabletId) {
        this.tabletId = tabletId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public InspecteeDto getInspectee() {
        return inspectee;
    }

    public void setInspectee(InspecteeDto inspectee) {
        this.inspectee = inspectee;
    }

    public Set<EntryDto> getEntries() {
        return entries;
    }

    public void setEntries(Set<EntryDto> entries) {
        this.entries = entries;
    }

    public Set<OtherEntryDto> getConventionalTags() {
        return conventionalTags;
    }

    public void setConventionalTags(Set<OtherEntryDto> conventionalTags) {
        this.conventionalTags = conventionalTags;
    }

    public Set<DocumentRecordDto> getScannedDocuments() {
        return scannedDocuments;
    }

    public void setScannedDocuments(Set<DocumentRecordDto> scannedDocuments) {
        this.scannedDocuments = scannedDocuments;
    }

    public InspectorCommentDto getInspector1() {
        return inspector1;
    }

    public void setInspector1(InspectorCommentDto inspector1) {
        this.inspector1 = inspector1;
    }

    public InspectorCommentDto getInspector2() {
        return inspector2;
    }

    public void setInspector2(InspectorCommentDto inspector2) {
        this.inspector2 = inspector2;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getTotalInRegistryForProducer() {
        return totalInRegistryForProducer;
    }

    public void setTotalInRegistryForProducer(int totalInRegistryForProducer) {
        this.totalInRegistryForProducer = totalInRegistryForProducer;
    }

}
