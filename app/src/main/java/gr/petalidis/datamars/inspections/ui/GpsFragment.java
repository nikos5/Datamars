/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.slf4j.Logger;

import java.util.Locale;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.utilities.WGS84Converter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GpsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GpsFragment extends Fragment {

    private static final String LATITUDE_KEY = "latitude";
    private static final String LONGITUDE_KEY = "longitude";

    private double latitude;
    private double longitude;

    private static final int PERMISSION_GPS_REQUEST_CODE = 2;

    private static final Logger log = Log4jHelper.getLogger(GpsFragment.class.getName());


    private Location gpsLocation = null;

    private boolean requestLocationUpdates = false;

    private LocationCallback mLocationCallback;

    private FusedLocationProviderClient mFusedLocationClient;

    public GpsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param latitude  The latitude of the initial gps location
     * @param longitude The longitude of the initial gps location
     * @return A new instance of fragment GpsFragment.
     */
    public static GpsFragment newInstance(double latitude, double longitude) {
        GpsFragment fragment = new GpsFragment();
        Bundle args = new Bundle();
        args.putDouble(LONGITUDE_KEY, longitude);
        args.putDouble(LATITUDE_KEY, latitude);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.latitude = getArguments().getDouble(LATITUDE_KEY);
            this.longitude = getArguments().getDouble(LONGITUDE_KEY);
        }
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    setGpsLocation(location);
                }
            }
        };
        // recovering the instance state
        checkLocationPermission();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gps, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ImageButton gpsButton = getActivity().findViewById(R.id.gpsButton);
        gpsButton.setOnClickListener(this::gpsToggle);

    }

    public void gpsToggle(View view) {
        ImageButton button = getActivity().findViewById(R.id.gpsButton);

        if (requestLocationUpdates) {
            stopLocationUpdates();
            requestLocationUpdates = false;
            Drawable locationOff = getResources().getDrawable(R.drawable.ic_baseline_location_off_48, null);
            button.setImageDrawable(locationOff);
        } else {
            requestLocationUpdates = true;
            checkLocationPermission();
            Drawable locationOn = getResources().getDrawable(R.drawable.ic_outline_location_on_48, null);
            button.setImageDrawable(locationOn);
        }
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    @Override
    public void onResume() {
        super.onResume();
        checkLocationPermission();
    }

    private void checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // TODO: Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("Αίτημα χρήσης υπηρεσιών τοποθεσίας")
                        .setMessage("Η τοποθεσία χρησιμοποιείται για την τοποθέτηση του στάβλου όταν καταχωρείτε έλεγχο")
                        .setPositiveButton(android.R.string.ok, (dialogInterface, i) ->
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_GPS_REQUEST_CODE))
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_GPS_REQUEST_CODE);
            }
        } else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), this::setGpsLocation);
            if (requestLocationUpdates) {
                LocationRequest currentLocationRequest = new LocationRequest();
                currentLocationRequest.setInterval(500)
                        .setFastestInterval(0)
                        .setMaxWaitTime(0)
                        .setSmallestDisplacement(0)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                if (mFusedLocationClient != null) {
                    mFusedLocationClient.requestLocationUpdates(currentLocationRequest, mLocationCallback, null);
                }
            } else {
                stopLocationUpdates();
            }
        }
    }

    public void setText(double latitude, double longitude, float accuracy) {
        TextView gpsLocationView = getView().findViewById(R.id.gpsValue);
        String message = String.format(Locale.forLanguageTag("el"), "%.2f", latitude)
                + ", " + String.format(Locale.forLanguageTag("el"), "%.2f", longitude);

        if (accuracy > 0) {
            message += "\n (ακρίβεια 68% εντός:"
                    + String.format(Locale.forLanguageTag("el"), "%.2f", accuracy) + "μ)";
        }
        gpsLocationView.setText(message);
    }

    private void setGpsLocation(Location location) {

        if (requestLocationUpdates && location != null &&
                (gpsLocation == null || location.distanceTo(gpsLocation) > 1 || location.getAccuracy() < gpsLocation.getAccuracy())) {
            gpsLocation = location;

            double[] coordinates;

            coordinates = WGS84Converter.toGGRS87(gpsLocation.getLatitude(), gpsLocation.getLongitude());

            float accuracy = location.getAccuracy();
            setText(coordinates[0], coordinates[1], accuracy);
            latitude = coordinates[0];
            longitude = coordinates[1];
            ((GpsUpdateLocation) getActivity()).setLocation(latitude, longitude);
        }
    }

    public void turnOffGps() {
        ImageButton button = getActivity().findViewById(R.id.gpsButton);

        stopLocationUpdates();
        requestLocationUpdates = false;
        Drawable locationOff = getResources().getDrawable(R.drawable.ic_baseline_location_off_48, null);
        button.setImageDrawable(locationOff);

    }

    public interface GpsUpdateLocation {
        void setLocation(double latitude, double longitude);
    }

}