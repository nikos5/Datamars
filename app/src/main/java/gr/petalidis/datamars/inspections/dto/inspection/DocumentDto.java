/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;


import java.util.UUID;

import gr.petalidis.datamars.inspections.dto.util.UUIDGenerator;

public class DocumentDto {

    private UUID id = UUIDGenerator.EMPTY_UUID;

    private byte[] document = new byte[0];

    public DocumentDto() {
    }

    public DocumentDto(UUID id) {
        this.id = id;
    }

    public DocumentDto(UUID id, byte[] document) {
        this.id = id;
        this.document = document;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

}
