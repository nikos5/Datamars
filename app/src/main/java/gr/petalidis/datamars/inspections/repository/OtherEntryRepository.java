/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.slf4j.Logger;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.inspections.domain.Inspectee;
import gr.petalidis.datamars.inspections.domain.OtherEntry;
import gr.petalidis.datamars.inspections.domain.OtherEntryType;

public class OtherEntryRepository {
    private static final Logger log = Log4jHelper.getLogger(OtherEntryRepository.class.getName());

    public static void deleteWithInspectionId(DbHandler dbHandler, UUID inspectionId) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            String[] selectionArgs = {inspectionId.toString()};
            int deleted = db.delete(DbHandler.TABLE_INSPECTION_OTHER_ENTRIES, "inspectionId = ?", selectionArgs);
            log.info("Deleted {} number of entries before saving ", deleted);

        }
    }
    public static void save(DbHandler dbHandler, Set<OtherEntry> entries)
    {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            entries.forEach(e -> {
                ContentValues values = new ContentValues();
                values.put("id", e.getId().toString());
                values.put("type", e.getEntryType().getName());
                values.put("inspectionId", e.getInspectionId().toString());
                values.put("producerTin", e.getInspectee().getTin());
                values.put("producerName", e.getInspectee().getName());
                values.put("animal", e.getAnimal());
                values.put("value", e.getCount());

                long returnValue = db.insertWithOnConflict(DbHandler.TABLE_INSPECTION_OTHER_ENTRIES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (returnValue == -1) {
                    String[] selectionArgs = {e.getId().toString()};
                    int updated = db.update(DbHandler.TABLE_INSPECTION_OTHER_ENTRIES, values, "id = ?", selectionArgs);
                    if (updated != 1) {
                        log.error("Unable to update entry {}", e.getId());
                    }
                }
            });

        }
    }

    static Set<OtherEntry> getEntriesFor(DbHandler dbHandler, UUID inspectionId) {
        Set<OtherEntry> entries = new HashSet<>();
        final String[] columns = {"id",
                "inspectionId",
                "type",
                "producerTin",
                "producerName",
                "animal",
                "value"};
        final String selection = "inspectionId = ?";
        final String[] selectionArgs = {inspectionId.toString()};

        try (final SQLiteDatabase db = dbHandler.getReadableDatabase();
             Cursor cursor = db.query(DbHandler.TABLE_INSPECTION_OTHER_ENTRIES, columns, selection, selectionArgs, null, null, null)) {

            if (cursor.moveToFirst()) {
                do {
                    OtherEntry entry = new OtherEntry();
                    entry.setId(UUID.fromString(cursor.getString(0)));
                    entry.setInspectionId(UUID.fromString(cursor.getString(1)));
                    entry.setEntryType(OtherEntryType.valueOf(cursor.getString(2)));
                    entry.setInspectee(new Inspectee(cursor.getString(3), cursor.getString(4)));
                    entry.setAnimal(cursor.getString(5));
                    entry.setCount(cursor.getInt(6));
                    entries.add(entry);
                } while (cursor.moveToNext());
            }
        }
        return entries;
    }


    public static void update(DbHandler dbHandler, Set<OtherEntry> entries) {
        try (final SQLiteDatabase db = dbHandler.getWritableDatabase()) {
            entries.forEach(e -> {
                ContentValues values = new ContentValues();
                String[] selectionArgs = {e.getId().toString()};
                values.put("type", e.getEntryType().getName());
                values.put("inspectionId", e.getInspectionId().toString());
                values.put("producerTin", e.getInspectee().getTin());
                values.put("producerName", e.getInspectee().getName());
                values.put("animal", e.getAnimal());
                values.put("value", e.getCount());

                int result = db.update(DbHandler.TABLE_INSPECTION_OTHER_ENTRIES, values, "id = ?", selectionArgs);
                if (result != 1) {
                    log.error("Unable to update entry {}", e.getId());
                }
            });

        }
    }
}
