/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto.inspection;

import java.io.Serializable;
import java.util.UUID;

import gr.petalidis.datamars.inspections.domain.OtherEntry;
import gr.petalidis.datamars.inspections.domain.OtherEntryType;
import gr.petalidis.datamars.inspections.dto.util.UUIDGenerator;

public class OtherEntryDto implements Serializable {

    private UUID id = UUIDGenerator.generate();

    private OtherEntryType entryType = OtherEntryType.CONVENTIONAL;

    private InspecteeDto inspectee = new InspecteeDto("", "");

    private String animal = "";

    private int count;

    public OtherEntryDto(OtherEntry otherEntry) {
        this.entryType = otherEntry.getEntryType();
        this.inspectee = new InspecteeDto(otherEntry.getInspectee().getTin(),
                otherEntry.getInspectee().getName());
        this.animal = otherEntry.getAnimal();
        this.count = otherEntry.getCount();

    }

    public OtherEntryDto() {

    }

    public InspecteeDto getInspectee() {
        return inspectee;
    }

    public void setInspectee(InspecteeDto inspectee) {
        this.inspectee = inspectee;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public OtherEntryType getEntryType() {
        return entryType;
    }

    public void setEntryType(OtherEntryType entryType) {
        this.entryType = entryType;
    }

}
