/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import java.util.ArrayList;

import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.service.UploadResult;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    private ArrayList<String> idList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            idList = savedInstanceState.getStringArrayList("inspections");
            if (idList == null) {
                idList = new ArrayList<>();
            }
        }
        Intent intent = getIntent();
        if (intent != null) {
            idList = intent.getStringArrayListExtra("inspections");
            if (idList == null) {
                idList = new ArrayList<>();
            }
        }
        setContentView(R.layout.activity_login);
        loginViewModel = new LoginViewModel(getApplication());

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText usernameEditText2 = findViewById(R.id.username2);

        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        if (!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Το δίκτυο δεν είναι διαθέσιμο!", Toast.LENGTH_LONG).show();
        }
        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }

                if (loginFormState.getUsername2Error() != null) {
                    usernameEditText2.setError(getString(loginFormState.getUsername2Error()));
                }

                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<UploadResult>() {
            @Override
            public void onChanged(@Nullable UploadResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                loginButton.setEnabled(true);
                if (!loginResult.isSuccess()) {
                    showLoginFailed(loginResult.getReason());
                } else {
                    updateUiWithUser(loginResult.getReason());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        usernameEditText2.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        usernameEditText2.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    loginButton.setEnabled(false);
                    loginViewModel.login(usernameEditText.getText().toString(),
                            usernameEditText2.getTransitionName(),
                            passwordEditText.getText().toString(),
                            idList, loadingProgressBar);
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginButton.setEnabled(false);
                loginViewModel.login(usernameEditText.getText().toString(),
                        usernameEditText2.getText().toString(),
                        passwordEditText.getText().toString(),
                        idList, loadingProgressBar);

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        idList = savedInstanceState.getStringArrayList("inspections");
        if (idList == null) {
            idList = new ArrayList<>();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putStringArrayList("inspections", idList);
    }

    private void updateUiWithUser(String reason) {
        Toast.makeText(getApplicationContext(), "Επιτυχία μεταφόρτωσης " + reason, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(String errorString) {
        Toast.makeText(getApplicationContext(), "Αποτυχία μεταφόρτωσης: " + errorString, Toast.LENGTH_SHORT).show();
    }
}