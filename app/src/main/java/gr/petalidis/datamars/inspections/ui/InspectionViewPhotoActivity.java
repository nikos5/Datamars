/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.Inspection;
import gr.petalidis.datamars.inspections.domain.ScannedDocument;
import gr.petalidis.datamars.inspections.dto.ThumbnailDto;
import gr.petalidis.datamars.inspections.repository.DbHandler;
import gr.petalidis.datamars.inspections.service.InspectionService;

public class InspectionViewPhotoActivity extends AppCompatActivity {
    private String pictureFilePath = "";
    private static final int REQUEST_PICTURE_CAPTURE = 1;
    private ImageView image;
    private List<ThumbnailDto> thumbnails = new ArrayList<>();

    private Inspection inspection;
    private static final String INSPECTION_KEY = "inspection";
    private static final Logger log = Log4jHelper.getLogger(InspectionViewPhotoActivity.class.getName());

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                PackageManager packageManager = this.getPackageManager();
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                            //Do Nothing
                        });
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo("gr.petalidis.datamars", 0);
                    String message = "Έκδοση: " + packageInfo.versionName + "\n" +
                            "Διατίθεται με άδεια ανοιχτού λογισμικού Apache 2.0\n" +
                            "βλ. https://gitlab.com/nikos5/Datamars/releases\n";
                    builder.setTitle("RECAP Livestock Inspector").setMessage(message);
                    builder.show();
                } catch (PackageManager.NameNotFoundException e) {
                    log.error("Could not show about message {}", e.getLocalizedMessage());
                }

                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_view_photo);
        image = findViewById(R.id.picture);
        ImageButton captureButton = findViewById(R.id.capture);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            captureButton.setEnabled(false);
        }
        captureButton.setOnClickListener(view -> this.sendTakePictureIntent());

        if (savedInstanceState != null) {
            inspection = (Inspection)savedInstanceState.getSerializable("inspection");
            pictureFilePath = savedInstanceState.getString("pictureFilePath");
            if (pictureFilePath != null && !pictureFilePath.equals("")) {
                File file = new File(pictureFilePath);
                image.setImageURI(Uri.fromFile(file));
            }
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable("inspection");
        }

        thumbnails = inspection.getScannedDocuments().stream().map(x->new ThumbnailDto(new File(x.getImagePath()).getName(),x.getImagePath())).collect(Collectors.toList());

        final LinearLayout linearLayout = findViewById(R.id.imageslist);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        thumbnails.forEach(item -> addImageToGalleryView(linearLayout, inflater, item));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    private void addImageToGalleryView(LinearLayout linearLayout, LayoutInflater inflater, ThumbnailDto item) {
        final int THUMBSIZE = 128;

        View view = inflater.inflate(R.layout.listimages,
                linearLayout, false);
        ImageView imageView = view
                .findViewById(R.id.img_preview);

        Bitmap imageBitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(item.getImagePath()),
                THUMBSIZE, THUMBSIZE);
        imageView.setImageBitmap(imageBitmap);

        imageView.setOnClickListener(event -> {
            File file = new File(item.getImagePath());
            image.setImageURI(Uri.fromFile(file));
            pictureFilePath = item.getImagePath();
        });
        TextView textView = view
                .findViewById(R.id.img_label);
        textView.setText(item.getName());
        linearLayout.addView(view);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("inspection", inspection);
        outState.putSerializable("pictureFilePath", pictureFilePath);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            //noinspection unchecked
            thumbnails = (ArrayList<ThumbnailDto>) savedInstanceState.getSerializable("thumbnails");
            pictureFilePath = savedInstanceState.getString("pictureFilePath");
            if (pictureFilePath != null && !pictureFilePath.equals("")) {
                File file = new File(pictureFilePath);
                image.setImageURI(Uri.fromFile(file));
            }
        } else {
            inspection = (Inspection) getIntent().getExtras().getSerializable("inspection");
        }
    }

    private void sendTakePictureIntent() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File pictureFile;
            try {
                pictureFile = getPictureFile();
                Uri photoURI = FileProvider.getUriForFile(this,
                        "gr.petalidis.datamars.fileprovider",
                        pictureFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File getPictureFile() throws IOException {
        String timeStamp = System.currentTimeMillis() + "";
        String pictureFile = "BOVSCANNER_" + timeStamp;
        ContextWrapper cw = new ContextWrapper(Moo.getAppContext());
        File storageDir = cw.getFilesDir();
        File imageFile = File.createTempFile(pictureFile, ".jpg", storageDir);
        pictureFilePath = imageFile.getAbsolutePath();
        return imageFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
            File imgFile = new File(pictureFilePath);
            if (imgFile.exists()) {
                ThumbnailDto thumbnailDto = new ThumbnailDto(imgFile.getName(), pictureFilePath);
                ScannedDocument scannedDocument = inspection.addThumbnail(thumbnailDto);
                if (inspection.hasValidProducers()) {
                    DbHandler dbHandler = new DbHandler(this.getApplicationContext());
                    InspectionService.updatePhotos(dbHandler, inspection, Collections.singletonList(scannedDocument));
                }
                image.setImageURI(Uri.fromFile(imgFile));
                thumbnails.add(thumbnailDto);
                final LinearLayout linearLayout = findViewById(R.id.imageslist);
                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                addImageToGalleryView(linearLayout, inflater, thumbnailDto);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InspectionViewActivity.class);
        intent.putExtra(INSPECTION_KEY, inspection);
        startActivity(intent);
    }
}