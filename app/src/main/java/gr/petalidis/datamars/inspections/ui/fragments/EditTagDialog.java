/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import gr.petalidis.datamars.R;
import gr.petalidis.datamars.inspections.domain.CommentType;
import gr.petalidis.datamars.inspections.domain.Entry;

public class EditTagDialog extends Dialog implements View.OnClickListener {
    private EditText countryText;
    private EditText editTag;
    private EditText ownerTag;
    private EditText editTag2;

    private final Entry item;
    private final List<Entry> items;

    public EditTagDialog(Context context, Entry item, List<Entry> items) {
        super(context);
        this.item = item;
        this.items = items;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_tag);//here is your xml with EditText and 'Ok' and 'Cancel' buttons
        Button btnOk = findViewById(R.id.okButton);
        countryText = findViewById(R.id.countryTag);
        countryText.setText(item.getCountry());
        countryText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 4) {
                    editTag.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                //NOTHING TO DO HERE
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                //NOTHING TO DO HERE
            }
        });
        String tag = item.getTag().replaceAll("-", "");
        editTag = findViewById(R.id.prefectureTag);
        editTag.setText(tag.substring(0, 4));
        editTag.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 4) {
                    ownerTag.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                //NOTHING TO DO HERE
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                //NOTHING TO DO HERE
            }
        });
        ownerTag = findViewById(R.id.ownerTag);
        ownerTag.setText(item.getOwner());
        ownerTag.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 4) {
                    editTag2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                //NOTHING TO DO HERE
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                //NOTHING TO DO HERE
            }
        });
        editTag2 = findViewById(R.id.editTag2);
        editTag2.setText(tag.substring(8));
        btnOk.setOnClickListener(this);

        Button cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(view -> dismiss());
    }

    @Override
    public void onClick(View v) {
        if (countryText.getText().length() != 4) {
            countryText.setError("Το μήκος πρέπει να είναι 4 ψηφία");
            return;
        }
        if (editTag.getText().length() != 4) {
            editTag.setError("Το μήκος πρέπει να είναι 4 ψηφία");
            return;
        }
        if (ownerTag.getText().length() != 4) {
            ownerTag.setError("Το μήκος πρέπει να είναι 4 ψηφία");
            return;
        }
        if (editTag2.getText().length() != 4) {
            editTag2.setError("Το μήκος πρέπει να είναι 4 ψηφία");
            return;
        }

        item.setCountry(countryText.getText().toString());
        item.setTag(editTag.getText().toString() + ownerTag.getText().toString() + editTag2.getText().toString());
        if (items.stream().anyMatch(x ->
                !x.getId().equals(item.getId())
                        && x.getCountry().equals(item.getCountry())
                        && x.getTag().equals(item.getTag()))) {
            item.setComment(CommentType.DOUBLE);
        }
        dismiss();
    }
}