/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import org.slf4j.Logger;

import java.io.IOException;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class LogoutApiClient {
    static final String BASE_URL = Moo.getProperty("server");
    private static final Logger log = Log4jHelper.getLogger(LogoutApiClient.class.getName());

    public static void logout(String jwt) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();

        LogoutApi service = retrofit.create(LogoutApi.class);

        Call<Integer> callSync = service.logout(jwt);

        Response<Integer> response = null;
        try {
            response = callSync.execute();
            if (response.isSuccessful()) {
                log.info("Επιτυχής έξοδος");
            } else {
                log.error("Ανεπιτυχής έξοδος");
            }
        } catch (IOException e) {
            log.error("Ανεπιτυχής έξοδος: {}", e.getLocalizedMessage());
        }


    }
}