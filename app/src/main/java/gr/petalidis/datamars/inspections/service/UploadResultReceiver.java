/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.service;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.widget.ProgressBar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.lang.ref.WeakReference;

public class UploadResultReceiver extends ResultReceiver {
    public static final int RESULT_CODE_OK = 200;
    public static final int RESULT_CODE_ERROR = 400;
    public static final int RESULT_CODE_PROGRESS = 211;
    public static final int RESULT_CODE_PROGRESS_START = 222;

    public static final String PARAM_EXCEPTION = "exception";
    public static final String PARAM_RESULT = "result";
    public static final String PARAM_PROGRESS = "progress";
    public static final String PARAM_TOTAL_SIZE = "total_size";

    private MutableLiveData<UploadResult> result = new MutableLiveData<>();
    private WeakReference<Context> activityRef;
    private WeakReference<ProgressBar> progressBarWeakReference;

    public UploadResultReceiver(Context context, Handler handler, ProgressBar progressBar) {
        super(handler);
        this.activityRef = new WeakReference<>(context);
        this.progressBarWeakReference = new WeakReference<>(progressBar);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultCode == RESULT_CODE_OK) {
            result.setValue(new UploadResult(true, ""));
        } else if (resultCode == RESULT_CODE_PROGRESS) {
            ProgressBar progressBar = progressBarWeakReference.get();
            if (progressBar != null) {
                progressBar.setProgress((int) resultData.getFloat(PARAM_PROGRESS));
            }
        } else if (resultCode == RESULT_CODE_PROGRESS_START) {
            ProgressBar progressBar = progressBarWeakReference.get();
            if (progressBar != null) {
                progressBar.setProgress(0);

            }
        } else {
            Exception e = (Exception) resultData.getSerializable(PARAM_EXCEPTION);
            result.setValue(new UploadResult(false, e.getLocalizedMessage()));
        }
    }

    public LiveData<UploadResult> getResult() {
        return result;
    }
}
