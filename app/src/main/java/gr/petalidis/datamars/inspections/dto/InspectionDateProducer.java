/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.inspections.dto;

import androidx.annotation.NonNull;

public class InspectionDateProducer {
    private final String id;
    private final String inspectionDate;
    private final String mainProducer;
    private final boolean finalised;
    private final boolean uploaded;


    public InspectionDateProducer(String uuid, String inspectionDate, String mainProducer, boolean finalised, boolean uploaded) {
        this.id = uuid;
        this.inspectionDate = inspectionDate;
        this.mainProducer = mainProducer;
        this.finalised = finalised;
        this.uploaded = uploaded;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public String getMainProducer() {
        return mainProducer;
    }

    public String getId() {
        return id;
    }

    public boolean isFinalised() {
        return finalised;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public boolean canBeUploaded() {
        return isFinalised() && !isUploaded();
    }

    @NonNull
    @Override
    public String toString() {
        return mainProducer;
    }

}
