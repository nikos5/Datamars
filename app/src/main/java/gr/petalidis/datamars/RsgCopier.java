/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


package gr.petalidis.datamars;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.widget.TextView;

import org.slf4j.Logger;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

import gr.petalidis.datamars.rsglibrary.Rsg;
import gr.petalidis.datamars.rsglibrary.RsgExporter;
import gr.petalidis.datamars.rsglibrary.RsgReader;
import gr.petalidis.datamars.rsglibrary.RsgRootDirectory;
import gr.petalidis.datamars.rsglibrary.RsgSession;
import gr.petalidis.datamars.rsglibrary.RsgSessionFiles;
import gr.petalidis.datamars.rsglibrary.RsgSessionScanner;

public class RsgCopier extends AsyncTask<Object, String, Integer> {
    private static final Logger log = Log4jHelper.getLogger(RsgCopier.class.getName());

    private final WeakReference<TextView> textView;

    public RsgCopier(TextView textView) {
        this.textView = new WeakReference<>(textView);
    }


    @Override
    protected Integer doInBackground(Object... params) {
        int numberOfSessionsLocated = 0;
        int numberOfFilesWritter = 0;

        try {
            UsbManager usbManager = (UsbManager) Objects.requireNonNull(Moo.getAppContext()).getSystemService(Context.USB_SERVICE);
            if (usbManager != null) {
                HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
                Collection<UsbDevice> ite = usbDevices.values();
                UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});
                for (UsbDevice usb : usbs) {
                    log.info("Connected usb device {}", usb.getDeviceName());
                    log.info("Connected usb class {}", usb.getDeviceClass());
                    log.info("Connected usb product name {}", usb.getProductName());
                    log.info("Connected usb product id {}", usb.getProductId());
                    log.info("Connected usb product manufacturer name {}", usb.getManufacturerName());
                    log.info("Connected usb product version name {}", usb.getVersion());
                    //log.info("Connected usb product serial name {}", usb.getSerialNumber());
                }
            }
            RsgRootDirectory rsgRootDirectory = new RsgRootDirectory();

            publishProgress("Datamars located");

            log.info("Found datamars under: {} ", rsgRootDirectory.rsgRoot());

            RsgSessionFiles rsgSessionFiles = RsgSessionScanner.scanDirectory(rsgRootDirectory.rsgRoot());
            numberOfSessionsLocated += rsgSessionFiles.getSessions().size();
            publishProgress(numberOfSessionsLocated + " sessions located");
            log.info("{} sessions located",numberOfSessionsLocated);

            for (RsgSession rsgSession : rsgSessionFiles.getSessions()) {
                try {
                    Set<Rsg> rsgs = RsgReader.readRsgFromScanner(rsgSession.getFilepath());
                    if (!rsgs.isEmpty()) {
                        String filename = RsgExporter.export(rsgs, rsgRootDirectory.getCsvDirectory(), rsgSession.getCsvName());
                        if (!filename.equals("")) {
                            publishProgress("Wrote file " + filename);
                            log.info("Wrote file: {}", filename);
                            numberOfFilesWritter++;
                        }
                    } else {
                        publishProgress("Skipped empty file " + rsgSession.getFilepath());
                    }
                } catch (ParseException e) {
                    log.error("Error {} reading file: {}", e.getMessage(), rsgSession.getFilepath());
                }
            }

            RsgSessionScanner.scanUsbDirectory(rsgRootDirectory.getCsvDirectory());
        } catch (IllegalStateException  | IOException | ParseException e) {
            log.error("Unable to scan Usb Directory: {}", e.getLocalizedMessage());
            publishProgress("Unable to scan Directory: " + e.getLocalizedMessage());
        }


        return numberOfFilesWritter;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        TextView actualView = textView.get();
        if (actualView != null) {
            actualView.setText(values[0]);
        }

    }


    @Override
    protected void onPostExecute(Integer numberOfFilesWritten) {
        super.onPostExecute(numberOfFilesWritten);

        TextView actualView = textView.get();
        if (actualView != null) {
            if (numberOfFilesWritten == 0) {
                actualView.setText(R.string.msg_failed_sync);
            } else {
                Moo.getAppContext();
                actualView.setText(Moo.getAppContext().getString(R.string.numberOfSyncedFiles, numberOfFilesWritten));
            }
        }


    }
}
