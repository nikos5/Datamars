/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars.rsglibrary;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

import org.slf4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import gr.petalidis.datamars.Log4jHelper;
import gr.petalidis.datamars.Moo;

public class RsgRootDirectory {
    private static final Logger log = Log4jHelper.getLogger(RsgRootDirectory.class.getName());

    private final CsvRootDirectory csvRootDirectory = new CsvRootDirectory();
    private String rsgRoot;

    private String usbName;

    private String csvPath;
    public RsgRootDirectory() {
        boolean result = false;
        if (Build.VERSION.SDK_INT >= 30) {

            log.info("I am external storage manager: {}", Environment.isExternalStorageManager());

            StorageManager sm = (StorageManager) Moo.getAppContext().
                    getSystemService(Context.STORAGE_SERVICE);



            sm.getRecentStorageVolumes().forEach(x->{
                            log.info("{} {} {}",x.getMediaStoreVolumeName(), x.getDescription(Moo.getAppContext()),
                                    x.getUuid());
                            log.info(x.toString());});


            List<StorageVolume> storageVolumes
                    = sm.getRecentStorageVolumes().stream()
                    .filter(x ->
                            x.isRemovable() &&
                                    !x.isPrimary() && (x.getState().equals(Environment.MEDIA_MOUNTED)
                                    || x.getState().equals(Environment.MEDIA_MOUNTED_READ_ONLY))
                                    &&
                                    x.getDescription(Moo.getAppContext()).equals("GES3S"))
                    .collect(Collectors.toList());


            if (storageVolumes.isEmpty()) {
                log.info("No GES3S was mounted. Found only the following {}", sm.getRecentStorageVolumes()
                        .stream().map(x -> x.getMediaStoreVolumeName()));

                throw new IllegalStateException("No GES3S was mounted");

            }
            for (StorageVolume sv : storageVolumes) {
                log.info("Media store volume name {}, isremovable {} state {}",
                        sv.getMediaStoreVolumeName(),
                        sv.isRemovable(),
                        sv.getState());
                if (sv.getDirectory() != null) {
                    log.info("Attempting to look under  {}", sv.getDirectory().getAbsolutePath());
                    result = findDatamars(sv.getDirectory()) || result;
                }
            }
        } else {
            log.info("Attempting to look under  {}", "/storage");
            result = findDatamars(new File("/storage"));
        }
        if (result == false) {
            throw new IllegalStateException("Could not find a connected Datamars device");
        }

    }

    boolean findDatamars(File startingPath) {

        log.info("RsgRootDirectory {}", startingPath.getAbsolutePath());

        List<String> rsgRoots = recurseIntoFilePath(new ArrayList<>(), startingPath);
        if (rsgRoots.size() != 1) {
            if (rsgRoots.size() > 1) {
                String paths = rsgRoots.stream().collect(Collectors.joining(","));
                log.error("Found multiple possible paths {}", paths);
            } else {
                log.error("Could not find a connected Datamars device {}", startingPath);
            }
            return false;
        }

        rsgRoot = rsgRoots.get(0);

        usbName = calcUsbName(rsgRoot);

        csvPath = calcCsvPath(usbName);
        return true;
    }

    private List<String> recurseIntoFilePath(List<String> results, File path) {

        File[] files = path.listFiles();

        if (files != null) {
            for (File inFile : files) {
                if (inFile.isDirectory() && inFile.canRead()) {
                    log.info("Looking into {}", inFile.getAbsolutePath());
                    if (inFile.getName().toLowerCase().startsWith("session")) {
                        results.add(inFile.getAbsolutePath());
                        return results;
                    }
                    recurseIntoFilePath(results, inFile);
                }
            }

        } else {
            log.info("listfiles() returned null for {}", path);
        }
        return results;
    }

    public String rsgRoot() {
        return rsgRoot;
    }

    public String getUsbName() {
        return usbName;
    }

    public String getCsvDirectory() {
        return csvPath;
    }

    private String calcUsbName(String usbPath)
    {
        String name = usbPath.replace("Session", "");

        String[] usbNames = name.split(File.separator);
        if (usbNames.length > 0) {
            name = usbNames[usbNames.length - 1];
        } else {
            name = "";
        }
        return name;
    }

    private String calcCsvPath(String usbName) {

        File topDirectory = new File(csvRootDirectory.getDirectory());

        if (!topDirectory.exists()) {
            boolean mkdir = topDirectory.mkdirs();
            if (!mkdir) {
                log.error("Could not create top level directory");
                throw new IllegalStateException("Could not create top level directory");
            }
        }
        File datamarsDir = new File(csvRootDirectory.getDirectory() + File.separator + usbName);
        if (!datamarsDir.exists()) {
            boolean mkDir = datamarsDir.mkdirs();
            if (!mkDir) {
                log.error("Could not create device-differentiating directory {}", datamarsDir.getAbsolutePath());
                throw new IllegalStateException("Could not create device-differentiating directory");
            }
        }

        return datamarsDir.getAbsolutePath();
    }

}
