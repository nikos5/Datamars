/*
 * Copyright 2017-2019 Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


package gr.petalidis.datamars.rsglibrary;

import org.slf4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import gr.petalidis.datamars.Log4jHelper;

class RsgParser {
    private final static Logger log = Log4jHelper.getLogger(RsgParser.class.getName());

    static List<Rsg> parseRsgStrings(List<String> rsgStrings) throws ParseException {
        List<Rsg> rsgs = new ArrayList<>();
        for (String rsgString: rsgStrings) {
            rsgs.add(parseRsgString(rsgString));
        }
        return rsgs;
    }

    static Rsg parseRsgString(String rsgString) throws ParseException {

        
        String []parts = rsgString.split("\\|");

        if (parts.length!=3 ) {
            //Try , instead of |
            parts = rsgString.split(",");
            if (parts.length!=3) {
                log.error("Unable to read rsg: {}", rsgString);
                throw new IllegalArgumentException("Parts not divided by | character");
            }
        }

        if (!parts[0].startsWith("[") && !parts[2].endsWith("]")) {
            log.error("Parts not starting/ending with []: {}", rsgString);
            throw new IllegalArgumentException("Parts not starting/ending with []");
        }

        parts[0] = parts[0].substring(1);
        parts[2] = parts[2].substring(0,parts[2].length()-1);
        return new Rsg(parts[0],parts[1],parts[2]);
    }

    static Rsg parseCsvString(String rsgString) throws ParseException {


        String []parts = rsgString.split(",");

        if (parts.length!=3 ) {
            log.error("Parts not divided by , character {}", rsgString);
            throw new IllegalArgumentException("Parts not divided by , character");
        }
        String dateString = fixDate(parts[2]);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hhmmss");

        Date date = format.parse(dateString);
        return new Rsg(parts[0],parts[1],date);
    }

    public static String fixDate(String part) throws IllegalArgumentException {
        String[] dateParts = part.split(" ");
        if (dateParts.length != 2) {
            log.error("Could not read time in string {}", part);
            throw new IllegalArgumentException("Could not read time in string " + part);
        }
        String time = dateParts[1];
        String date = dateParts[0];

        if (time.length() > "hhmmss".length() || time.length() < "hms".length()) {
            log.error("Could not read time in string {}", part);
            throw new IllegalArgumentException("Could not read time in string " + part);
        }
        if (time.length() == "hms".length()) {
            //Everything is single digit!
            String hour = time.substring(0, 1);
            String minutes = time.substring(1, 2);
            String secs = time.substring(2, 3);
            time = "0" + hour + "0" + minutes + "0" + secs;
        } else if (time.length() == "hmmss".length()) {
            time = "0" + time;
        } else if (time.length() == "hmms".length()) {
            //Hour is single digit Either mins or secs is single digit
            //Assume mins is two digits unless mins>59
            String hour = time.substring(0, 1);
            String minutes = time.substring(1, 3);
            String seconds = time.substring(3, 4);

            try {
                int min = Integer.parseInt(minutes);
                if (min > 59) {
                    minutes = "0" + time.substring(1, 2);
                    seconds = time.substring(2, 4);
                    int secs = Integer.parseInt(seconds);
                    if (secs > 59) {
                        log.error("Could not read time in string {}", part);
                        throw new IllegalArgumentException("Could not read time in string " + part);
                    }
                } else {
                    seconds = "0" + seconds;
                }
                time = "0" + hour + minutes + seconds;
            } catch (NumberFormatException e) {
                log.error("Could not read time in string {}", part);
                throw new IllegalArgumentException("Could not read time in string " + part);
            }
        }

        return date + " " + time;
    }
}
