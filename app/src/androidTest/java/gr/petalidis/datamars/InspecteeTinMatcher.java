/*
 * Copyright 2017-2020 OPEKEPE
 * Authored by Nikolaos Petalidis
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package gr.petalidis.datamars;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import gr.petalidis.datamars.inspections.domain.Inspectee;

public class InspecteeTinMatcher extends TypeSafeMatcher<Inspectee> {
    private final String tinExpected;

    private InspecteeTinMatcher(String tinExpected) {
        this.tinExpected = tinExpected;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Tin did not contain a value of " + tinExpected);
    }

    @Override
    protected boolean matchesSafely(Inspectee producer) {
        return producer.getTin().equals(tinExpected);
    }

    public static InspecteeTinMatcher hasTin(String tinExpected) {
        return new InspecteeTinMatcher(tinExpected);
    }
}
